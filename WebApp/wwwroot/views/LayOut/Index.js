function getUrlCurrent() {
    var url = window.location;
    // URL API
    var APIURL = url.protocol + "//" + url.hostname + ":" + url.port;

    return APIURL
}

function getApiAjaxJson(url, type, data) {
    let urlBase = getUrlCurrent()

    if (data) {
        return $.ajax({
            type: type,
            url: urlBase + url,
            contentType: "application/json; charset=utf-8",
            data: data
        });
    }

    return $.ajax({
        type: type,
        url: urlBase + url,
        contentType: "application/json; charset=utf-8"
    });
}

function getApiAjaxAuthorizeToken(url, type, token) {
    let urlBase = getUrlCurrent()
    
    return $.ajax({
        type: type,
        url: url,
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Token', token);
        },
    });
}

function getApiAjaxAuthorize(url, type, data, token) {
    let urlBase = getUrlCurrent()

    if (data) {
        return $.ajax({
            type: type,
            url: urlBase + url,
            contentType: "application/json; charset=utf-8",
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization',
                    "Bearer " + token);
            },
            data: data,
        });
    }

    return $.ajax({
        type: type,
        url: urlBase + url,
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization',
                "Bearer " + token);
        },
    });
}
function getApiAjaxAuthMap(url, type, data, token) {
    let urlBase = getUrlCurrent()

    if (data) {
        return $.ajax({
            type: type,
            url: url,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', token);
                xhr.setRequestHeader('Content-Type', "application/json");
            },
            data: data,
        });
    }

    return $.ajax({
        type: type,
        url: url,
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', token);
            xhr.setRequestHeader('Content-Type', "application/json");
        },
    });
}
function getApiAjaxUploadFile(url, type, file) {
    let urlBase = getUrlCurrent()

    return $.ajax({
        url: urlBase + url,
        method: type,
        data: file,
        processData: false,
        contentType: false,
    })
}
function isCheckStringEmpty(str) {
    if (str == null || str.trim().length === 0) {
        return true
    }
    return false
}
function isCheckLengthNumber(number, min, max) {
    if (number >= min && number <= max) {
        return true
    }
    return false
}
function formatDatePhoTo() {
    var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hour = d.getHours(),
        minute = d.getMinutes(),
        second = d.getSeconds()

    if (month.length < 2) month = '0' + month
    if (day.length < 2) day = '0' + day
    if (hour.length < 2) hour = '0' + hour
    if (minute.length < 2) minute = '0' + minute
    if (second.length < 2) second = '0' + second

    return [year, month, day, hour, minute, second].join('')
}
function formatDatetime(date) {
    if (date != null && date != "") {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear()

        if (month.length < 2) month = '0' + month
        if (day.length < 2) day = '0' + day

        return [day, month, year].join('/');
    }

    return "";
};