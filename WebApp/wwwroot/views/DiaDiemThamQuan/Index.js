﻿const url = window.location;
const APIURL = url.protocol + "//" + url.hostname + ":" + url.port;
var pathFolder = "~/FileUpload/AvatarLocation";
var pathThuMuc = "/FileUpload/AvatarLocation";
var rs = '';

//FILE
var fileTepKemTheoNameNew = '';
var fileTepKemTheoNameOld = [];
var nameTepKemTheo = '';
var formData = new FormData();

//AVATA
var nameAnhDaiDienNew = '';
var nameAnhDaiDienOld = '';
var idEditFilePond = '';

//CKeditor
var gioiThieuEditCK = null;
var diaDiemEditCK = null;

const FILETYPE = ['application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf', 'application/vnd.ms-excel', `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`];
(function ($) {
    $.fn.inputFilter = function (callback, errMsg) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop focusout", function (e) {
            if (callback(this.value)) {
                // Accepted value
                if (["keydown", "mousedown", "focusout"].indexOf(e.type) >= 0) {
                    $(this).removeClass("input-error");
                    this.setCustomValidity("");
                }
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                // Rejected value - restore the previous one
                $(this).addClass("input-error");
                this.setCustomValidity(errMsg);
                this.reportValidity();
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                // Rejected value - nothing to restore
                this.value = "";
            }
        });
    };
}(jQuery));

$(document).ready(function () {
    let valueOption = [];
    let valueOptionEdit = [];
    LanguageSelect();
    $("#input-search").keyup(function (event) {
        if (event.keyCode === 13) {
            $('#dataGridLocation').DataTable().ajax.reload().draw();
        }
    });
    $('#select-language').select2({
        placeholder: "Tất cả",
        theme: 'bootstrap4',
        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
        language: "vi",
        allowClear: true
    }).on('select2:unselect', function (e) {
        // Lấy giá trị đã bị xóa
        var removedValue = e.params.data.id;

        // Xóa đoạn HTML tương ứng dựa trên giá trị đã bị xóa
        $('#ngon-ngu-select').find(`#collapseThuyetMinh${removedValue}`).parent().remove();
        // Xóa giá trị đã bị xóa khỏi mảng valueOption
        var index = valueOption.indexOf(removedValue);
        if (index > -1) {
            valueOption.splice(index, 1);
        }

         if (valueOption.length === 0) {
        // If the array is empty, remove all collapse elements
        $('#ngon-ngu-select').find('.collapse').parent().remove();
    }
    });

    $('#select-language-edit').select2({
        placeholder: "Tất cả",
        theme: 'bootstrap4',
        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
        language: "vi",
        allowClear: true
    }).on('select2:unselect', function (e) {
        // Lấy giá trị đã bị xóa
        var removedValue = e.params.data.id;

        // Xóa đoạn HTML tương ứng dựa trên giá trị đã bị xóa
        $('#ngon-ngu-select-edit').find(`#collapseThuyetMinh${removedValue}`).parent().remove();
        // Xóa giá trị đã bị xóa khỏi mảng valueOptionEdit
        var index = valueOptionEdit.indexOf(removedValue);
        if (index > -1) {
            valueOptionEdit.splice(index, 1);
        }

    });

    function buildData() {
        let request = $('#input-search').val();
        let laguage1 = $('#search-language').val();
        const res = {
            "tuKhoa": request ?? "",
            "ngonNguID": laguage1 ?? "1",
        }
        return res;
    };


    $('#dataGridLocation').dataTable({
        "autoWidth": false,
        "ordering": false,
        "bInfo": false,
        "bLengthChange": false,
        "filter": false,
        "language": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Hiện: _MENU_",
            "sZeroRecords": "Không có dữ liệu",
            "sEmptyTable": "Bảng trống",
            "sInfo": "Hiện dòng _START_ đến _END_ trong tổng _TOTAL_ dòng",
            "sInfoEmpty": "Hiện dòng 0 đến 0 trong tổng 0 dòng",
            "sSearch": "Tìm kiếm",
            "sLoadingRecords": "Đang tải...",
            "paginate": {
                next: '<span class="material-icons-outlined">chevron_right</span>',
                previous: '<span class="material-icons-outlined">chevron_left</span>'
            }
        },
        "ajax": {
            type: "POST",
            url: APIURL + "/API/DiemThamQuan/Gets",
            data: buildData,
            dataSrc: function (data) {
                for (let i = 0; i < data.resultObj.length; i++) {
                    data.resultObj[i].STT = i + 1;
                }
                return data.resultObj;
            },
        },
        "columnDefs": [
            {
                targets: 5,
                render: function (data, type, row, meta) {
                    return '<span data-toggle="tooltip" title="Chỉnh sửa" class="icon edit-command-btn" id=n-"' + meta.row + '"><i class="fa-regular fa-pen-to-square"></i></span><span data-toggle="tooltip" title="Xóa" class="icon delete-command-btn" id=n-"' + meta.row + '"><i class="fa-regular fa-trash-can"></i></span>';
                }
            }
        ],
        "columns": [
            { "data": "STT", "width": "40px", "class": "STT-text center-align" },
            { "data": "tenDiem", "width": "200px", "class": "left-align text-medium text-blue" },
            { "data": "banKinhQuyUoc", "width": "100px", "class": "center-align" },
            { "data": "diaDiem", "width": "300px", "class": "left-align" },
            { "data": "thuTu", "width": "100px", "class": "center-align" },
            { "data": "ID", "width": "110px", "class": "center-align group-icon-action" },
        ]
    });

    $('#btn-search').click(function () {
        $('#dataGridLocation').DataTable().ajax.reload().draw()
    });

    $("#dataGridLocation").on('click', '.delete-command-btn', function () {
        var id = $(this).attr("ID").match(/\d+/)[0];
        var data = $('#dataGridLocation').DataTable().row(id).data();

        let fileName = data.anhDaiDien.split("/")[3];

        $('#idDiaDiem').val(data.diemThamQuanID);
        $('#tenDiaDiem').text(data.tenDiem);
        $('#idNoiDung').val(data.noiDungID);
        $('#avataName').val(fileName);
        
        $('#modalDelete').modal('show');
    })

    $("#dataGridLocation").on('click', '.edit-command-btn', function () {
        var id = $(this).attr("ID").match(/\d+/)[0];
        var data = $('#dataGridLocation').DataTable().row(id).data();
        console.log(data);
        nameAnhDaiDienOld = data.anhDaiDien.substring(data.anhDaiDien.lastIndexOf("/") + 1);
        $("#idDiemThamQuanEdit").val(data.diemThamQuanID);
        $("#kinhDoEdit").val(data.kinhDo);
        $("#viDoEdit").val(data.viDo);
        $("#banKinhEdit").val(data.banKinhQuyUoc);
        $("#thuTuEdit").val(data.thuTu);
        $("#avataEdit").prop('src',data.anhDaiDien);
        NgonNguSelectEdit(data.diemThamQuanID);
        LanguageSelectEdit(data.diemThamQuanID);
        $("#modalEdit").modal("show");
    })

    $('.check-num').inputFilter(function (value) {
        return /^\d*$/.test(value);
    }, "Dữ liệu nhập vào phải là số."); 

    $('#select-language').change(function () {
        valueOption = [];
        // Lặp qua tất cả các giá trị đã chọn trong select
        $('#select-language option:selected').each(function () {
            valueOption = ($(this).val()); // Thêm giá trị vào mảng
        });

        // Gọi hàm NgonNguSelect2 với mảng valueOption
        NgonNguSelect2(valueOption);
    });

    $('#select-language-edit').change(function () {
        valueOptionEdit = [];
        // Lặp qua tất cả các giá trị đã chọn trong select
        $('#select-language-edit option:selected').each(function () {
            valueOptionEdit = ($(this).val()); // Thêm giá trị vào mảng
        });

        // Gọi hàm NgonNguSelect2 với mảng valueOptionEdit
        NgonNguSelect3(valueOptionEdit);
    });
})

function LanguageSelect() {
    $.ajax({
        url: APIURL + "/API/NgonNgu/Gets",
        type: "GET",
        data: {
            'TuKhoa': '',
        },
        success: function (item) {
            $(`#select-language`).empty();
            $('#search-language').append(`<option class="form-control" value=${item.resultObj[0].ngonNguID} selected disabled>${item.resultObj[0].tenNgonNgu}</option>`);
            if (item && item.resultObj) {
                item.resultObj.forEach(function (data) {
                    $('#select-language').append(`<option class="form-control" value=${data.ngonNguID}>${data.tenNgonNgu}</option>`);
                    $('#search-language').append(`<option class="form-control" value=${data.ngonNguID}>${data.tenNgonNgu}</option>`);
                })
            }
        }
    })
}

function LanguageSelectEdit(id) {
    $.ajax({
        url: APIURL + "/API/NgonNgu/GetByDiemThamQuanNotExist?id=" + id,
        type: "GET",
        success: function (item) {
            $(`#select-language-edit`).empty();
            if (item && item.resultObj) {
                item.resultObj.forEach(function (data) {
                    $('#select-language-edit').append(`<option class="form-control" value=${data.ngonNguID}>${data.tenNgonNgu}</option>`);
                })
            }
        }
    })
}

function NgonNguSelect2(valueOption) {

    if (valueOption.length !== 0) {
        $.ajax({
            url: APIURL + "/API/NgonNgu/GetByID?id=" + valueOption,
            type: "GET",
            success: function (data) {
                if (data && data.resultObj) {
                    let rs = data.resultObj

                    let html = `
                          <div class="accordion-item mt-3">
                                <div class="accordion-header">
                                    <h6 class="title-accordion">
                                        <span class="icon icon-document"></span>
                                          <a class="btn btn-primary" data-bs-toggle="collapse" href="#collapseThuyetMinh${rs.ngonNguID}" role="button" aria-expanded="false" aria-controls="collapseThuyetMinh${rs.ngonNguID}">
                                           ${rs.tenNgonNgu}
                                          </a>
                                    </h6>
                                </div>
                                <div class="collapse" id="collapseThuyetMinh${rs.ngonNguID}">
                                  <div class="card card-body">
                                      <input type='hidden' value='${rs.ngonNguID}' class='idNgonNgu'/>
                                      <div>
                                          <label for="tenDiemAdd">Tên điểm tham quan <span class='red-text'>*</span></label>
                                          <input type='text' id='tenDiemAdd' class='form-control tenDiemAdd' maxlength='100'/>
                                      </div>
                                      <div>
                                          <label for="diaDiemAdd">Địa điểm <span class='red-text'>*</span></label>
                                          <textarea id="diaDiemAdd" class="diaDiemAdd" name="diaDiemAdd" rows="4" cols="50"></textarea>
                                      </div>
                                      <div>
                                          <label for="gioiThieuAdd">Giới thiệu <span class='red-text'>*</span></label>
                                          <textarea id="gioiThieuAdd" class="gioiThieuAdd" name="gioiThieuAdd" rows="4" cols="50"></textarea>
                                      </div>
                                      <div>
                                          <label for="tepKemTheoAdd">Tệp kèm theo</label>
                                          <input type='file' id="tepKemTheoAdd" class="tepKemTheoAdd" accept=".pdf, .doc, .docx, .xls, .xlsx" />
                                     </div>
                                  </div>
                                </div>
                            </div>

            `;
                    $('#ngon-ngu-select').append(html);
                }
            }
        })
    }
}

function NgonNguSelect3(valueOption) {

    if (valueOption.length !== 0) {
        $.ajax({
            url: APIURL + "/API/NgonNgu/GetByID?id=" + valueOption,
            type: "GET",
            success: function (data) {
                if (data && data.resultObj) {
                    let rs = data.resultObj
                    let html = ` <div class="accordion-item accordion-item-edit mt-3">
                                <div class="accordion-header">
                                    <h6 class="title-accordion">
                                        <span class="icon icon-document"></span>
                                          <a class="btn btn-primary" data-bs-toggle="collapse" href="#collapseThuyetMinhEdit${rs.ngonNguID}" role="button" aria-expanded="false" aria-controls="collapseThuyetMinh${rs.ngonNguID}">
                                           ${rs.tenNgonNgu}
                                          </a>
                                    </h6>
                                </div>
                                <div class="collapse collapse-edit" id="collapseThuyetMinhEdit${rs.ngonNguID}">
                                  <div class="card card-body">
                                      <input type="hidden" id="idNoiDungEdit${rs.ngonNguID}" class="idNoiDungEdit"/>
                                      <input type='hidden' value='${rs.ngonNguID}' class='idNgonNguEdit'/>
                                      <div>
                                          <label for="tenDiemEdit${rs.ngonNguID}">Tên điểm tham quan <span class='red-text'>*</span></label>
                                          <input type='text' id='tenDiemEdit${rs.ngonNguID}' class='form-control tenDiemEdit' maxlength='100'/>
                                      </div>
                                      <div>
                                          <label for="diaDiemEdit${rs.ngonNguID}">Địa điểm <span class='red-text'>*</span></label>
                                          <textarea id="diaDiemEdit${rs.ngonNguID}" class="diaDiemEdit" name="diaDiemEdit" rows="4" cols="50"></textarea>
                                      </div>
                                      <div>
                                          <label for="gioiThieuEdit${rs.ngonNguID}">Giới thiệu <span class='red-text'>*</span></label>
                                          <textarea id="gioiThieuEdit${rs.ngonNguID}" class="gioiThieuEdit" name="gioiThieuEdit" rows="4" cols="50"></textarea>
                                      </div>
                                      <div>
                                          <label for="tepKemTheoEdit${rs.ngonNguID}">Tệp kèm theo</label>
                                          <input type='file' id="tepKemTheoEdit${rs.ngonNguID}" class="tepKemTheoEdit" accept=".pdf, .doc, .docx, .xls, .xlsx" />
                                     </div>
                                     <div id="list-file${rs.ngonNguID}"></div>
                                  </div>
                                </div>
                            </div>`;
                    $('#ngon-ngu-select-edit').append(html);
                }
            }
        })
    }
}

function NgonNguSelectEdit(idDiemThamQuan) {
    $('#ngon-ngu-select-edit').empty();
    var editorElement = [];
    $.ajax({
        url: APIURL + "/API/NgonNgu/GetByDiemThamQuanDoExist?id=" + idDiemThamQuan,
        type: "GET",
        success: function (item) {
            var html = ``;
            item.resultObj.forEach(function (data) {
                editorElement.push(data.ngonNguID);
                html += ` <div class="accordion-item accordion-item-edit mt-3">
                                <div class="accordion-header">
                                    <h6 class="title-accordion">
                                        <span class="icon icon-document"></span>
                                          <a class="btn btn-primary" data-bs-toggle="collapse" href="#collapseThuyetMinhEdit${data.ngonNguID}" role="button" aria-expanded="false" aria-controls="collapseThuyetMinh${data.ngonNguID}">
                                           ${data.tenNgonNgu}
                                          </a>
                                    </h6>
                                </div>
                                <div class="collapse collapse-edit" id="collapseThuyetMinhEdit${data.ngonNguID}">
                                  <div class="card card-body">
                                      <input type="hidden" id="idNoiDungEdit${data.ngonNguID}" class="idNoiDungEdit"/>
                                      <input type='hidden' value='${data.ngonNguID}' class='idNgonNguEdit'/>
                                      <div>
                                          <label for="tenDiemEdit${data.ngonNguID}">Tên điểm tham quan <span class='red-text'>*</span></label>
                                          <input type='text' id='tenDiemEdit${data.ngonNguID}' class='form-control tenDiemEdit' maxlength='100'/>
                                      </div>
                                      <div>
                                          <label for="diaDiemEdit${data.ngonNguID}">Địa điểm <span class='red-text'>*</span></label>
                                          <textarea id="diaDiemEdit${data.ngonNguID}" class="diaDiemEdit" name="diaDiemEdit" rows="4" cols="50"></textarea>
                                      </div>
                                      <div>
                                          <label for="gioiThieuEdit${data.ngonNguID}">Giới thiệu <span class='red-text'>*</span></label>
                                          <textarea id="gioiThieuEdit${data.ngonNguID}" class="gioiThieuEdit" name="gioiThieuEdit" rows="4" cols="50"></textarea>
                                      </div>
                                      <div>
                                          <label for="tepKemTheoEdit${data.ngonNguID}">Tệp kèm theo</label>
                                          <input type='file' id="tepKemTheoEdit${data.ngonNguID}" class="tepKemTheoEdit" accept=".pdf, .doc, .docx, .xls, .xlsx" />
                                     </div>
                                     <div id="list-file${data.ngonNguID}"></div>
                                  </div>
                                </div>
                            </div>`

            })
            $('#ngon-ngu-select-edit').append(html);

           
        }
    }).done(function(dataNgonNgu){
        $.ajax({
            type: "GET",
            async: false,
            url: APIURL + '/API/NoiDungThamQuan/GetByDiemThamQuan?id=' + idDiemThamQuan,
            success: function(data) {
                // Tạo một đối tượng để lưu trữ dữ liệu theo ngonNguID
                const ngonNguData = {};

                // Duyệt qua mảng data.resultObj và lưu trữ dữ liệu theo ngonNguID
                data.resultObj.forEach(item => {
                    const ngonNguID = item.ngonNguID;
                    if (!ngonNguData[ngonNguID]) {
                        ngonNguData[ngonNguID] = [];
                    }
                    ngonNguData[ngonNguID].push(item);
                });

                // Sau đó, bạn có thể sử dụng dữ liệu tương ứng với ngonNguID trong mảng dataNgonNgu
                dataNgonNgu.resultObj.forEach(ngonNgu => {
                    const ngonNguID = ngonNgu.ngonNguID;
                    const correspondingData = ngonNguData[ngonNguID];

                    if (correspondingData) {
                        $(`#idNoiDungEdit${correspondingData[0].ngonNguID}`).val(correspondingData[0].noiDungID);
                        $(`#idNgonNguEdit${correspondingData[0].ngonNguID}`).val(correspondingData[0].ngonNguID);
                        $(`#tenDiemEdit${correspondingData[0].ngonNguID}`).val(correspondingData[0].tenDiem);
                        $(`#diaDiemEdit${correspondingData[0].ngonNguID}`).val(correspondingData[0].diaDiem);
                        $(`#gioiThieuEdit${correspondingData[0].ngonNguID}`).val(correspondingData[0].gioiThieu);
                    }
                })
            }
        }).done(function (dataNoiDung) {
            dataNoiDung.resultObj.forEach(function (item) {
                $.ajax({
                    type: "GET",
                    async: false,
                    url: APIURL + '/API/TepKemTheo/GetByChuTheID?id=' + item.noiDungID,
                    success: function (data) {
                        if (data.resultObj) {
                            var idChuTheTep = data.resultObj.chuTheID;
                            if (item.noiDungID == idChuTheTep) {
                                $(`#list-file${item.ngonNguID}`).empty();
                                if (data.resultObj != null && data.resultObj != "") {
                                    let filename = data.resultObj.lienKet.substring(data.resultObj.lienKet.lastIndexOf('/') + 1);
                                    let html2 = `<a href="${APIURL}${data.resultObj.lienKet}" download><span class="text-blue text-medium">${filename}</span></a>`
                                    $(`#list-file${item.ngonNguID}`).append(html2);
                                    fileTepKemTheoNameOld.push(filename);
                                }
                            }
                        }
                    }
                })
            })
           
        })

        //editorElement.forEach(function (item) {
        //    //CKeditor
        //    //diaDiemEditCK = CKEDITOR.replace(`diaDiemEdit${item}`, {
        //    //    // Tùy chọn cấu hình ở đây
        //    //    language: 'vi', // Thiết lập ngôn ngữ là Tiếng Việt
        //    //    height: 300, // Thiết lập chiều cao trình soạn thảo
        //    //    toolbar: [ // Tùy chỉnh thanh công cụ
        //    //        { name: 'document', items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print'] },
        //    //        { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
        //    //        { name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll'] },
        //    //        { name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'SpecialChar'] },
        //    //        '/',
        //    //        { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
        //    //        { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'] },
        //    //        { name: 'links', items: ['Link', 'Unlink'] },
        //    //        { name: 'styles', items: ['Styles', 'Format'] },
        //    //    ]
        //    //});

        //    //gioiThieuEditCK = CKEDITOR.replace(`gioiThieuEdit${item}`, {
        //    //    // Tùy chọn cấu hình ở đây
        //    //    language: 'vi', // Thiết lập ngôn ngữ là Tiếng Việt
        //    //    height: 300, // Thiết lập chiều cao trình soạn thảo
        //    //    toolbar: [ // Tùy chỉnh thanh công cụ
        //    //        { name: 'document', items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print'] },
        //    //        { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
        //    //        { name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll'] },
        //    //        { name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'SpecialChar'] },
        //    //        '/',
        //    //        { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
        //    //        { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'] },
        //    //        { name: 'links', items: ['Link', 'Unlink'] },
        //    //        { name: 'styles', items: ['Styles', 'Format'] },
        //    //    ]
        //    //});
        //    //var pondTepKemTheoEdit = null;
        //    //FilePond.registerPlugin(FilePondPluginFileValidateType);
        //    ////UPLOAD
        //    //var fieldsetElementTepKemTheoEdit = document.querySelector(`#tepKemTheoEdit${item}`);
        //    //pondTepKemTheoEdit = FilePond.create(fieldsetElementTepKemTheoEdit, { acceptedFileTypes: FILETYPE });
        //    //pondTepKemTheoEdit.setOptions({
        //    //    labelIdle: `<span class="icon icon-upload"></span>
        //    //    <span class="d-block">Kéo và thả file ở đây</span>
        //    //    <span class="d-block">hoặc</span>
        //    //    <span class="filepond--label-action">Chọn file từ máy tính</span>
        //    //    <span class="d-block type-file">* Chỉ cho phép các đuôi tệp .doc, .docx, .pdf, .xls, .xlsx</span>`,
        //    //    labelFileTypeNotAllowed: 'Định dạng file không phù hợp',
        //    //    fileValidateTypeLabelExpectedTypes: ''
        //    //});

        //    //pondTepKemTheoEdit.on('addfile', (e, file) => {
        //    //    let fileupload = pondTepKemTheoEdit.getFile();
        //    //    console.log({ fileupload });
        //    //    let type = FILETYPE.find(x => x == fileupload.file.type)

        //    //    if (type) {
        //    //        nameTepKemTheo = `${file.file.name}`
        //    //        formData.append(nameTepKemTheo, file.file, nameTepKemTheo);
        //    //    } else {
        //    //        showNotification(false, "File upload không đúng định dạng!");
        //    //    }
        //    //})
        //})

    })
}

function addData() {
    // Điểm tham quan
    let kinhDo = $('#kinhDoAdd').val();
    let viDo = $('#viDoAdd').val();
    let banKinh = $('#banKinhAdd').val();
    let anhDaiDien = $("#anhDaiDienAdd")[0].files[0];
    let thuTu = $('#thuTuAdd').val();

    if (!anhDaiDien) {
        alert("Ảnh đại diện chưa được tải!");
        return;
    }

    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif|\.bmp|\.webp)$/i;
    if (!allowedExtensions.exec(anhDaiDien.name)) {
        alert("Loại tệp ảnh đại diện không hợp lệ!");
        return;
    }

    if (checkEmptyBlank(kinhDo) || checkEmptyBlank(viDo) || checkEmptyBlank(banKinh) || checkEmptyBlank(thuTu)) {
        alert("Thông tin điểm tham quan bắt buộc không được để trống!");
        return;
    }

    let isCheckKinhDo = validateInputRegex(kinhDo, new RegExp('^[0-9.-]*$'))
    let isCheckViDo = validateInputRegex(viDo, new RegExp('^[0-9.-]*$'))

    if (isCheckKinhDo == false || isCheckViDo == false) {
        alert("Kinh độ, vĩ độ không hợp lệ!");
        return;
    }

    if (parseFloat(kinhDo) > 180 || parseFloat(kinhDo) < -180) {
        alert("Kinh độ phải nằm trong khoảng -180 đến 180!");
        return;
    }

    if (parseFloat(viDo) > 90 || parseFloat(viDo) < -90) {
        alert("Vĩ độ phải nằm trong khoảng -90 đến 90!");
        return;
    }

    var hasFilledCollapse = false; 
    var hasAlertShown = false;
    // Kiểm tra thông tin từng collapse
    $('.accordion-item').each(function () {
        var collapse = $(this).find('.collapse');

        function areAllInputsEmpty() {
            var inputs = collapse.find('.tenDiemAdd, .diaDiemAdd, .gioiThieuAdd');
            for (var i = 0; i < inputs.length; i++) {
                if (inputs.eq(i).val() === '') {
                    return false;
                }

            }
            return true; 
        }

        if (!areAllInputsEmpty() && !hasAlertShown) {
            alert('Thông tin nội dung không được để trống trống!');
            hasAlertShown = true;
            return;
        } else {
            hasFilledCollapse = true;
        }
    });

    let namefile = dateTimeFile() + "_" + anhDaiDien.name;

    if (hasFilledCollapse) {
        var dataDiemThamQuan = {
            'KinhDo': kinhDo,
            'ViDo': viDo,
            'BanKinhQuyUoc': banKinh,
            'AnhDaiDien': `${pathThuMuc}/${namefile}`,
            'ThuTu': thuTu,
        }

         //Thực hiện AJAX để thêm Điểm tham quan
        $.ajax({
            type: 'POST',
            async: false,
            url: APIURL + '/API/DiemThamQuan/Add',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(dataDiemThamQuan),
            success: function () {
                AddAvataLocation(anhDaiDien);
            }
        }).done(function (item) {
            var hasAlertShown = false; // Biến để kiểm tra đã hiển thị thông báo hay chưa

            var dataNoiDungArray = []; // Mảng để lưu trữ dữ liệu của các collapse có dữ liệu đầy đủ

            $('.accordion-item').each(function () {
                var collapse = $(this).find('.collapse');
                var idNgonNgu = collapse.find('.idNgonNgu').val();
                var tenDiemAdd = collapse.find('.tenDiemAdd').val();
                var diaDiemAdd = collapse.find('.diaDiemAdd').val();
                var gioiThieuAdd = collapse.find('.gioiThieuAdd').val();

                if (tenDiemAdd && diaDiemAdd && gioiThieuAdd) {
                    // Nếu có dữ liệu đầy đủ trong collapse, thêm dữ liệu vào mảng dataNoiDungArray
                    const dataNoiDung = {
                        "NgonNguID": idNgonNgu,
                        "DiemThamQuanID": item.diemThamQuanID,
                        "TenDiem": tenDiemAdd,
                        "DiaDiem": diaDiemAdd,
                        "GioiThieu": gioiThieuAdd,
                    };
                    dataNoiDungArray.push(dataNoiDung);
                }
            });

            if (dataNoiDungArray.length > 0) {
                // Thực hiện cuộc gọi AJAX cho từng collapse có dữ liệu đầy đủ
                var index = 0;
                function addTepKemTheo() {
                    if (index < dataNoiDungArray.length) {
                        const dataNoiDung = dataNoiDungArray[index];
                        $.ajax({
                            type: 'POST',
                            async: false,
                            url: APIURL + '/API/NoiDungThamQuan/Add',
                            contentType: 'application/json; charset=utf-8',
                            data: JSON.stringify(dataNoiDung),
                            success: function (temp) {
                                // Trích xuất nội dung của tepKemTheoAdd
                                var collapse = $('.accordion-item').eq(index).find('.collapse');
                                var tepKemTheoContent = collapse.find('.tepKemTheoAdd')[0].files[0];
                                var noiDungID = temp.noiDungID; // Lấy noiDungID từ kết quả AJAX trước
                                if (noiDungID && tepKemTheoContent) {
                                    var nameTepKemTheo = dateTimeFile() + "_" + tepKemTheoContent.name;
                                    const dataTepKemTheo = {
                                        "ChuTheID": noiDungID,
                                        "MoTa": nameTepKemTheo,
                                        "LienKet": `${pathThuMuc}/${nameTepKemTheo}`,
                                    };

                                    // Thực hiện AJAX để thêm TepKemTheo
                                    $.ajax({
                                        type: 'POST',
                                        async: false,
                                        url: APIURL + '/API/TepKemTheo/Add',
                                        contentType: 'application/json; charset=utf-8',
                                        data: JSON.stringify(dataTepKemTheo),
                                        success: function () {
                                            // Tăng chỉ số và thêm TepKemTheo cho dữ liệu tiếp theo
                                            index++;
                                            addTepKemTheo();
                                            AddAvataLocation(tepKemTheoContent);
                                        }
                                    });
                                } else {
                                    // Nếu không có nội dung hoặc không có noiDungID, chuyển sang dữ liệu tiếp theo
                                    index++;
                                    addTepKemTheo();
                                }
                            }
                        });
                    } else {
                        // Tất cả dữ liệu đã được xử lý
                        $("#modalAdd").modal("hide");
                        closeModal();
                        $('#dataGridLocation').DataTable().ajax.reload().draw();
                    }
                }

                // Bắt đầu xử lý đầu tiên
                addTepKemTheo();
            } else {
                hasAlertShown = true; // Đánh dấu đã hiển thị thông báo
            }
        });
    }
}

function editData() {
    let idEdit = $("#idDiemThamQuanEdit").val();
    let kinhDo = $("#kinhDoEdit").val();
    let viDo = $("#viDoEdit").val();
    let banKinh = $("#banKinhEdit").val();
    let thuTu = $("#thuTuEdit").val();
    let anhDaiDien = $("#anhDaiDienEdit")[0].files[0];
    let flag = false;

    if (typeof anhDaiDien !== 'undefined') {
        nameAnhDaiDienNew = dateTimeFile() + "_" + anhDaiDien.name;
        flag = true;
        var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif|\.bmp|\.webp)$/i;
        if (!allowedExtensions.exec(anhDaiDien.name)) {
            alert("Loại tệp ảnh đại diện không hợp lệ!");
            return;
        }
    } else { 
        nameAnhDaiDienNew = nameAnhDaiDienOld;
       
    }

    if (checkEmptyBlank(kinhDo) || checkEmptyBlank(viDo) || checkEmptyBlank(banKinh) || checkEmptyBlank(thuTu)) {
        alert("Thông tin điểm tham quan bắt buộc không được để trống!");
        return;
    }

    let isCheckKinhDo = validateInputRegex(kinhDo, new RegExp('^[0-9.-]*$'))
    let isCheckViDo = validateInputRegex(viDo, new RegExp('^[0-9.-]*$'))

    if (isCheckKinhDo == false || isCheckViDo == false) {
        alert("Kinh độ, vĩ độ không hợp lệ!");
        return;
    }

    if (parseFloat(kinhDo) > 180 || parseFloat(kinhDo) < -180) {
        alert("Kinh độ phải nằm trong khoảng -180 đến 180!");
        return;
    }

    if (parseFloat(viDo) > 90 || parseFloat(viDo) < -90) {
        alert("Vĩ độ phải nằm trong khoảng -90 đến 90!");
        return;
    }

    var hasFilledCollapse = false;
    var hasAlertShown = false;
    // Kiểm tra thông tin từng collapse
    $('.accordion-item-edit').each(function () {
        var collapse = $(this).find('.collapse-edit');
        function areAllInputsEmpty() {
            var inputs = collapse.find('.tenDiemEdit, .diaDiemEdit, .gioiThieuEdit');
            for (var i = 0; i < inputs.length; i++) {
                if (inputs.eq(i).val() === '') {
                    return false;
                }

            }
            return true;
        }

        if (!areAllInputsEmpty() && !hasAlertShown) {
            alert('Thông tin nội dung không được để trống trống!');
            hasAlertShown = true;
            return;
        } else {
            hasFilledCollapse = true;
        }
    });

    if (hasFilledCollapse) {
        var dataDiemThamQuan = {
            'DiemThamQuanID': idEdit,
            'KinhDo': kinhDo,
            'ViDo': viDo,
            'BanKinhQuyUoc': banKinh,
            'AnhDaiDien': `${pathThuMuc}/${nameAnhDaiDienNew}`,
            'ThuTu': thuTu,
        }

        $.ajax({
            type: "PUT",
            async: false,
            url: APIURL + '/API/DiemThamQuan/Edit',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(dataDiemThamQuan),
            success: function () {
                if (flag) {
                    AddAvataLocation(anhDaiDien);
                    DeleteFileLocation(nameAnhDaiDienOld);

                }
            }
        }).done(function (item) {
            var dataNoiDungArray = []; // Mảng để lưu trữ dữ liệu của các collapse có dữ liệu đầy đủ
            var allItemsProcessed = false;
            var flagDataAdd = false;
            $('.accordion-item-edit').each(function (index) {
                var collapse = $(this).find('.collapse-edit');
                var idNgonNgu = collapse.find('.idNgonNguEdit').val();
                var tenDiemEdit = collapse.find('.tenDiemEdit').val();
                var diaDiemEdit = collapse.find('.diaDiemEdit').val();
                var gioiThieuEdit = collapse.find('.gioiThieuEdit').val();
                var idNoiDungEdit = collapse.find('.idNoiDungEdit').val();
               
                if (tenDiemEdit && diaDiemEdit && gioiThieuEdit && idNoiDungEdit) {
                    // Nếu có dữ liệu đầy đủ trong collapse, thêm dữ liệu vào mảng dataNoiDungArray
                    const dataNoiDung = {
                        "NgonNguID": idNgonNgu,
                        "DiemThamQuanID": item.diemThamQuanID,
                        "TenDiem": tenDiemEdit,
                        "DiaDiem": diaDiemEdit,
                        "GioiThieu": gioiThieuEdit,
                        "NoiDungID": idNoiDungEdit,
                    };
                    dataNoiDungArray.push(dataNoiDung);
                }

                if (idNoiDungEdit == '' && tenDiemEdit && diaDiemEdit && gioiThieuEdit) {
                    if (tenDiemEdit && diaDiemEdit && gioiThieuEdit) {
                        const dataNoiDungAdd = {
                            "NgonNguID": idNgonNgu,
                            "DiemThamQuanID": item.diemThamQuanID,
                            "TenDiem": tenDiemEdit,
                            "DiaDiem": diaDiemEdit,
                            "GioiThieu": gioiThieuEdit,
                        };
                        var collapse = $('.accordion-item-edit').eq(index).find('.collapse-edit');
                        var tepKemTheoContent = collapse.find('.tepKemTheoEdit')[0].files[0];
                        const dataNoiDung = dataNoiDungAdd;
                        console.log(tepKemTheoContent);
                        console.log(dataNoiDung);
                        //$.ajax({
                        //    type: 'POST',
                        //    async: false,
                        //    url: APIURL + '/API/NoiDungThamQuan/Add',
                        //    contentType: 'application/json; charset=utf-8',
                        //    data: JSON.stringify(dataNoiDung),
                        //    success: function (temp) {
                        //        var noiDungID = temp.noiDungID; // Lấy noiDungID từ kết quả AJAX trước
                        //        if (noiDungID && tepKemTheoContent) {
                        //            var nameTepKemTheo = dateTimeFile() + "_" + tepKemTheoContent.name;
                        //            const dataTepKemTheo = {
                        //                "ChuTheID": noiDungID,
                        //                "MoTa": nameTepKemTheo,
                        //                "LienKet": `${pathThuMuc}/${nameTepKemTheo}`,
                        //            };
                        //            console.log({ dataTepKemTheo });
                        //            // Thực hiện AJAX để thêm TepKemTheo
                        //            $.ajax({
                        //                type: 'POST',
                        //                async: false,
                        //                url: APIURL + '/API/TepKemTheo/Add',
                        //                contentType: 'application/json; charset=utf-8',
                        //                data: JSON.stringify(dataTepKemTheo),
                        //                success: function () {
                        //                    // Tăng chỉ số và thêm TepKemTheo cho dữ liệu tiếp theo
                        //                    AddAvataLocation(tepKemTheoContent);
                        //                }
                        //            });
                        //        }
                        //    }
                        //})
                        flagDataAdd = true;
                    } else {
                        hasAlertShown = true; // Đánh dấu đã hiển thị thông báo
                    }

                    if (index === $('.accordion-item-edit').length - 1) {
                        // Đây là lần cuối cùng trong vòng lặp
                        allItemsProcessed = true;
                    }
                }
            });

            if (dataNoiDungArray.length > 0) {
                // Thực hiện cuộc gọi AJAX cho từng collapse có dữ liệu đầy đủ
                var index = 0;
                function editTepKemTheo() {
                    if (index < dataNoiDungArray.length) {
                        const dataNoiDung = dataNoiDungArray[index];
                        $.ajax({
                            type: 'PUT',
                            async: false,
                            url: APIURL + '/API/NoiDungThamQuan/Edit',
                            contentType: 'application/json; charset=utf-8',
                            data: JSON.stringify(dataNoiDung),
                            success: function (temp) {
                             
                                var noiDungID = temp.noiDungID; // Lấy noiDungID từ kết quả AJAX trước
                                if (noiDungID) {
                                    $.ajax({
                                        type: "GET",
                                        async: false,
                                        url: APIURL + '/API/TepKemTheo/GetByChuTheID?id=' + noiDungID,
                                        success: function (tempFile) {
                                            console.log({ tempFile });
                                            if (tempFile.resultObj != null) {
                                                // Trích xuất nội dung của tepKemTheoAdd
                                                var collapse = $('.accordion-item-edit').eq(index).find('.collapse-edit');
                                                var tepKemTheoContent = collapse.find('.tepKemTheoEdit')[0].files[0];
                                                var flagFile = false;
                                                var tempFileOldToDelete = '';
                                                if (typeof tepKemTheoContent !== 'undefined') {
                                                    fileTepKemTheoNameNew = dateTimeFile() + "_" + tepKemTheoContent.name;
                                                    flagFile = true;
                                                    fileTepKemTheoNameOld.forEach(function (itemNameFile) {
                                                        if (tempFile.resultObj.moTa == itemNameFile) {
                                                            tempFileOldToDelete = itemNameFile;
                                                        }
                                                    })
                                                } else {
                                                    fileTepKemTheoNameOld.forEach(function (itemNameFile) {
                                                        if (tempFile.resultObj.moTa == itemNameFile) {
                                                            fileTepKemTheoNameNew = itemNameFile;
                                                        }
                                                    })
                                                }
                                                const dataTepKemTheo = {
                                                    "TepID": tempFile.resultObj.tepID,
                                                    "MoTa": fileTepKemTheoNameNew,
                                                    "LienKet": `${pathThuMuc}/${fileTepKemTheoNameNew}`,
                                                };
                                                 //Thực hiện AJAX để thêm TepKemTheo
                                                $.ajax({
                                                    type: 'PUT',
                                                    async: false,
                                                    url: APIURL + '/API/TepKemTheo/Edit',
                                                    contentType: 'application/json; charset=utf-8',
                                                    data: JSON.stringify(dataTepKemTheo),
                                                    success: function () {
                                                        // Tăng chỉ số và thêm TepKemTheo cho dữ liệu tiếp theo
                                                        if (flagFile) {
                                                            AddAvataLocation(tepKemTheoContent);
                                                            DeleteFileLocation(tempFileOldToDelete);
                                                        }
                                                        index++;
                                                        editTepKemTheo();
                                                    }
                                                });

                                            }
                                            index++;
                                            editTepKemTheo();
                                        }
                                    })
                                } else {
                                    // Nếu không có nội dung hoặc không có noiDungID, chuyển sang dữ liệu tiếp theo
                                    index++;
                                    editTepKemTheo();
                                }
                            }
                        });
                    }
                    //else {
                    //    // Tất cả dữ liệu đã được xử lý
                    //    $("#modalEdit").modal("hide");
                    //    $('#dataGridLocation').DataTable().ajax.reload().draw();
                    //}
                }

                // Bắt đầu xử lý đầu tiên
                editTepKemTheo();
            } else {
                hasAlertShown = true; // Đánh dấu đã hiển thị thông báo
            }
            if (allItemsProcessed && flagDataAdd) {
                $("#modalEdit").modal("hide");
                closeModal();
                $('#dataGridLocation').DataTable().ajax.reload().draw();
            }
        })
    }
}

function deleteData() {
    let idDiaDiem = $("#idDiaDiem").val();
    let tenAnhDaiDien = $("#avataName").val();
    let idNoiDung = $("#idNoiDung").val();
    $.ajax({
        type: "GET",
        async: false,
        url: APIURL + '/API/NoiDungThamQuan/GetByDiemThamQuan?id=' + idDiaDiem,
        success: function () {

        }
    }).done(function (data) {
        data.resultObj.forEach(function (item) {
            $.ajax({
                type: "GET",
                async: false,
                url: APIURL + '/API/TepKemTheo/GetByChuTheID?id=' + item.noiDungID,
                success: function () {

                }
            }).done(function (data) {
             
                $.ajax({
                    type: "DELETE",
                    async: false,
                    url: APIURL + '/API/TepKemTheo/Delete?id=' + data.resultObj.chuTheID,
                    success: function () {
                        DeleteFileLocation(data.resultObj.moTa)
                        console.log("xóa tệp kèm theo thành công");
                    }
                }).done(function () {
                    $.ajax({
                        type: "DELETE",
                        async: false,
                        url: APIURL + '/API/NoiDungThamQuan/DeleteByDiemThamQuanID?id=' + idDiaDiem,
                        success: function () {

                        }
                    })
                }).done(function () {
                    $.ajax({
                        type: "DELETE",
                        async: false,
                        url: APIURL + '/API/DiemThamQuan/Delete?id=' + idDiaDiem,
                        success: function () {
                            DeleteFileLocation(tenAnhDaiDien)

                            $("#modalDelete").modal("hide");
                            $('#dataGridLocation').DataTable().ajax.reload().draw();
                        }
                    })
                })
            })
        })
    })
}

function AddAvataLocation(file) {
    var formData = new FormData();
    var filename = `${dateTimeFile()}_${file.name}`;
    formData.append('file', file, filename);

    $.ajax({
        type: "POST",
        async: false,
        url: APIURL + '/API/DiemThamQuan/ChangeAvataLocation',
        data: formData,
        contentType: false,
        processData: false,
        success: function (data) {
            rs = data;
        }
    });
    return rs;
}

function DeleteFileLocation(name) {
    $.ajax({
        type: "DELETE",
        async: false,
        url: APIURL + '/API/DiemThamQuan/DeleteAvataLocation?fileName=' + name,
        contentType: false,
        processData: false,
        success: function () {
            
        }
    });
}

function readURLAnhDaiDienAdd(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#anhDaiDien')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function readURLAnhDaiDienEdit(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#avataEdit')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function checkEmptyBlank(str) {
    if (str == null || str.trim().length === 0) {
        return true
    }
    return false
};

function validateInputRegex(value, regex) {
    return regex.test(value);
}

function closeModal() {
    $('#kinhDoAdd').val("");
    $('#viDoAdd').val("");
    $('#banKinhAdd').val("");
    $("#anhDaiDien").prop('src', '/images/add_photo_alternate_2.jpg');
    $("#anhDaiDienAdd").val("");
    $('#thuTuAdd').val("");
    $('#select-language').val(null).trigger('change');
    $('#ngon-ngu-select').find('.collapse').parent().remove();

    $(".tenDiemAdd").val("");
    $(".diaDiemAdd").val("");
    $(".gioiThieuAdd").val("");
    $(".tepKemTheoAdd").val("");
    $(".collapse").collapse('hide');
}

function dateTimeFile() {
    let now = new Date();
    let day = String(now.getDate()).padStart(2, '0'); // Lấy ngày (01 - 31)
    let month = String(now.getMonth() + 1).padStart(2, '0'); // Lấy tháng (01 - 12)
    let year = now.getFullYear(); // Lấy năm
    let hours = String(now.getHours()).padStart(2, '0'); // Lấy giờ (00 - 23)
    let minutes = String(now.getMinutes()).padStart(2, '0'); // Lấy phút (00 - 59)
    let seconds = String(now.getSeconds()).padStart(2, '0'); // Lấy giây (00 - 59)

    let time = day + month + year + hours + minutes + seconds;

    return time;
} 