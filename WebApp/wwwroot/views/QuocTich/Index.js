﻿const url = window.location;
const APIURL = url.protocol + "//" + url.hostname + ":" + url.port;

$(document).ready(function () {
    $("#input-search").keyup(function (event) {
        if (event.keyCode === 13) {
            $('#dataGridNationality').DataTable().ajax.reload().draw();
        }
    });
    function buildData() {
        let request = $('#input-search').val();

        const data = {
            "tuKhoa": request,
        }
        return data;
    }

    $("#dataGridNationality").dataTable({
        "autoWidth": false,
        "ordering": false,
        "bInfo": false,
        "bLengthChange": false,
        "filter": false,
        "language": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Hiện: _MENU_",
            "sZeroRecords": "Không có dữ liệu",
            "sEmptyTable": "Bảng trống",
            "sInfo": "Hiện dòng _START_ đến _END_ trong tổng _TOTAL_ dòng",
            "sInfoEmpty": "Hiện dòng 0 đến 0 trong tổng 0 dòng",
            "sSearch": "Tìm kiếm",
            "sLoadingRecords": "Đang tải...",
            "paginate": {
                next: '<span class="material-icons-outlined">chevron_right</span>',
                previous: '<span class="material-icons-outlined">chevron_left</span>'
            }
        },
        "ajax": {
            url: APIURL + "/API/QuocTich/Gets",
            type: "GET",
            data: JSON.stringify(buildData()),
            dataSrc: function (data) {
                for (let i = 0; i < data.resultObj.length; i++) {
                    data.resultObj[i].STT = i + 1;
                }
                return data.resultObj;
            },
        },
        "columnDefs": [
            {
                targets: 2,
                render: function (data, type, row, meta) {
                    return '<span data-toggle="tooltip" title="Chỉnh sửa" class="icon edit-command-btn" id=n-"' + meta.row + '"><i class="fa-regular fa-pen-to-square"></i></span><span data-toggle="tooltip" title="Xóa" class="icon delete-command-btn" id=n-"' + meta.row + '"><i class="fa-regular fa-trash-can"></i></span>';
                }
            }
        ],
        "columns": [
            { "data": "STT", "width": "40px", "class": "STT-text center-align" },
            { "data": "tenQuocTich", "width": "100px", "class": "left-align text-medium text-blue" },
            { "data": "ID", "width": "110px", "class": "center-align group-icon-action" },
        ]
    })

    $('#btn-search').click(function () {
        $('#dataGridNationality').DataTable().ajax.reload().draw()
    });

    $('#dataGridNationality').on('click', '.edit-command-btn', function () {
        var id = $(this).attr("ID").match(/\d+/)[0];
        var data = $('#dataGridNationality').DataTable().row(id).data();

        $('#tenEdit').val(data.tenQuocTich);
        $('#idQuocTich').val(data.quocTichID);

        $('#modalEdit').modal('show');
    });

    $('#dataGridNationality').on('click', '.delete-command-btn', function () {
        var id = $(this).attr("ID").match(/\d+/)[0];
        var data = $('#dataGridNationality').DataTable().row(id).data();
        console.log({data});
        $('#tenQuocTich').text(data.tenQuocTich);
        $('#idQuocTich').val(data.quocTichID);

        $('#modalDelete').modal('show');
    });
})

function addData() {
    var tenQuocTich = $('#tenAdd').val();
    if (!checkEmptyBlank(tenQuocTich)) {
        const check = {
            'TenQuocTich': tenQuocTich,
            'Action': 0,
        }
        $.ajax({
            type: 'POST',
            async: false,
            url: APIURL + '/API/quocTich/CheckName',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(check),
            success: function (dt) {
                if (dt == 0) {
                    const data = {
                        "TenQuocTich": tenQuocTich,
                    }

                    $.ajax({
                        type: "POST",
                        async: false,
                        url: APIURL + '/API/QuocTich/Add',
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify(data),
                        success: function (data) {
                            $('#modalAdd').modal('hide');
                            closeModal();

                            $('#dataGridNationality').DataTable().ajax.reload().draw();
                        }
                    })
                } else {
                    alert("Tên quốc tịch đã tồn tại!");
                }
            }
        })
    }
}

function editData() {
    var tenQuocTich = $('#tenEdit').val();
    var idQuocTich = $('#idQuocTich').val();
    if (!checkEmptyBlank(tenQuocTich)) {
        const check = {
            'QuocTichID': idQuocTich,
            'TenQuocTich': tenQuocTich,
            'Action': 1,
        }
        $.ajax({
            type: 'POST',
            async: false,
            url: APIURL + '/API/quocTich/CheckName',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(check),
            success: function (dt) {
                if (dt == 0) {
                    const data = {
                        "TenQuocTich": tenQuocTich,
                        "QuocTichID": idQuocTich,
                    }
                    console.log(data);
                    $.ajax({
                        type: "PUT",
                        async: false,
                        url: APIURL + '/API/QuocTich/Edit',
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify(data),
                        success: function (data) {
                            $('#modalEdit').modal('hide');

                            $('#dataGridNationality').DataTable().ajax.reload().draw();
                        }
                    })
                } else {
                    alert("Tên quốc tịch đã tồn tại!");
                }
            }
        })
    }
}

function deleteData() {
    var id = $('#idQuocTich').val();
    if (id) {
        $.ajax({
            type: 'delete',
            async: false,
            url: APIURL + '/API/QuocTich/Delete?id=' + id,
            success: function () {
                $('#modalDelete').modal("hide");
                $('#dataGridNationality').DataTable().ajax.reload().draw()
            },
            error: function (err) {
                console.log("Lỗi: ", err);
            }
        })
    }
}

function closeModal() {
    $("#tenAdd").val("");
}

function checkEmptyBlank(str) {
    if (str == null || str.trim().length === 0) {
        return true
    }
    return false
};