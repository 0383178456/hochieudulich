﻿using Data.Models;
using Microsoft.AspNetCore.Mvc;
using Services;

namespace WebApp.Api
{
    [Route("API/[controller]/[action]")]
    [ApiController]
    public class TepKemTheoController : ControllerBase
    {
        private readonly ITepKemTheoService _tepKemTheoService;
        public TepKemTheoController(ITepKemTheoService tepKemTheoService)
        {
            _tepKemTheoService = tepKemTheoService;
        }

        [HttpPost]
        public async Task<IActionResult> Add(TepKemTheoParam data)
        {
            var result = await _tepKemTheoService.Add(data);
            return Ok(result);
        }

        [HttpPut]
        public async Task<IActionResult> Edit(TepKemTheo data)
        {
            var result = await _tepKemTheoService.Edit(data);
            return Ok(result);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _tepKemTheoService.Delete(id);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetByChuTheID(Guid id)
        {
            var result = await _tepKemTheoService.GetByChuTheID(id);
            return Ok(result);
        }
    }
}
