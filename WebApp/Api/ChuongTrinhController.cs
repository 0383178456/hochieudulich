﻿using Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services;
using System.Threading.Tasks;

namespace WebApp.Api
{
    [ApiController]
    [Route("API/[controller]/[action]")]
    public class ChuongTrinhController : ControllerBase
    {
        private readonly IChuongTrinhService _chuongTrinhService;
        public ChuongTrinhController(IChuongTrinhService chuongTrinhService)
        {
            _chuongTrinhService = chuongTrinhService;
        }

        //[Authorize(AuthenticationSchemes = "Bearer")]
        [HttpGet]
        public async Task<IActionResult> Gets()
        {
            var dd = Directory.GetCurrentDirectory();
            var result = await _chuongTrinhService.Gets();
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetByID(int id)
        {
            var result = await _chuongTrinhService.GetByID(id);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Add(ChuongTrinh data)
        {
            var result = await _chuongTrinhService.Add(data);
            return Ok(result);
        }

        [HttpPut]
        public async Task<IActionResult> Edit(ChuongTrinh data)
        {
            var result = await _chuongTrinhService.Edit(data);
            return Ok(result);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _chuongTrinhService.Delete(id);
            return Ok(result);
        }
    }
}
