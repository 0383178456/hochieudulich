﻿using Data;
using Data.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Services;
using System.Security.Claims;
       
namespace WebApp.Api
{
    [Route("/API/[controller]/[action]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ThamQuanDiaDiemController : ControllerBase
    {
        private readonly UserManager<HC_IdentityUser> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IThamQuanDiaDiemService _thamQuanDiaDiemService;
        public ThamQuanDiaDiemController(IThamQuanDiaDiemService thamQuanDiaDiemService
            , UserManager<HC_IdentityUser> userManager
            , IHttpContextAccessor httpContextAccessor)
        {
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
            _thamQuanDiaDiemService = thamQuanDiaDiemService;
        }

        [HttpGet]
        public async Task<IActionResult> Gets()
        {
            var result = await _thamQuanDiaDiemService.Gets();
            if (result != null)
            {
                return Ok(new ApiSuccessResult<IEnumerable<ThamQuanDiaDiem>>(result, "Lấy danh sách check in địa điểm tham quan thành công!"));
            }
            return Ok(new ApiErrorResult<bool>("Danh sách check in địa điểm tham quan trống!"));
        }

        [HttpPost("/API/CheckIn/Checked")]
        public async Task<IActionResult> Create(ThamQuanDiaDiemParam request)
        {
            if (!_httpContextAccessor.HttpContext.User.Identity.IsAuthenticated)
            {
                return Ok(new ApiErrorResult<bool>("Bạn chưa đăng nhập!"));
            }

            var userIdClaim = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Sid);
            var user = await _userManager.FindByIdAsync(userIdClaim.Value);
            if (user == null)
            {
                return Ok(new ApiErrorResult<bool>("Không tìm thấy người dùng!"));
            }

            ThamQuanDiaDiem req = new ThamQuanDiaDiem
            {
                DuKhachID = user.HoChieuDuKhachID,
                LichTrinhID = request.LichTrinhID
            };

            var result = await _thamQuanDiaDiemService.Create(req);

            return Ok(new ApiSuccessResult<ThamQuanDiaDiem>(result, "Check in thành công!"));
        }
    }
}
