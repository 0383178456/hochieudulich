﻿using Data;
using Microsoft.AspNetCore.Mvc;
using Services;
using System.Threading.Tasks;

namespace WebApp.Api
{
    [Route("API/[controller]/[action]")]
    [ApiController]
    public class NgonNguController : ControllerBase
    {
        private readonly INgonNguService _ngonNguService;
        public NgonNguController(INgonNguService ngonNguService)
        {
            _ngonNguService = ngonNguService;
        }

        [HttpGet]
        public async Task<IActionResult> Gets(string? tuKhoa)
        {
            var result = await _ngonNguService.Gets(tuKhoa);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetByID(int id)
        {
            var result = await _ngonNguService.GetByID(id);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Add(NgonNgu data)
        {
            var result = await _ngonNguService.Add(data);
            return Ok(result);
        }

        [HttpPut]
        public async Task<IActionResult> Edit(NgonNgu data)
        {
            var result = await _ngonNguService.Edit(data);
            return Ok(result);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _ngonNguService.Delete(id);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CheckName(NgonNguCheck data)
        {
            var result = await _ngonNguService.CheckName(data);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetByDiemThamQuanNotExist(Guid id)
        {
            var result = await _ngonNguService.GetByDiemThamQuanNotExist(id);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetByDiemThamQuanDoExist(Guid id)
        {
            var result = await _ngonNguService.GetByDiemThamQuanDoExist(id);
            return Ok(result);
        }
    }
}
