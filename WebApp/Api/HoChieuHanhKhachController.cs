﻿using Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Services;
using System.Globalization;

namespace WebApp.Api
{
    [Route("/API/[controller]/[action]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "root")]
    public class HoChieuHanhKhachController : ControllerBase
    {
        private SemaphoreSlim semaphore = new SemaphoreSlim(1);
        private readonly IHoChieuHanhKhachService _hoChieuHanhKhachService;
        private readonly UserManager<HC_IdentityUser> _userManager;
        public HoChieuHanhKhachController(IHoChieuHanhKhachService hoChieuHanhKhachService
            , UserManager<HC_IdentityUser> userManager)
        {
            _hoChieuHanhKhachService = hoChieuHanhKhachService;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _hoChieuHanhKhachService.Gets();
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Filter([FromForm] HoChieuHanhKhachFilter filter)
        {
            var result = await _hoChieuHanhKhachService.Filter(filter);
            if (result != null)
            {
                return Ok(new ApiSuccessResult<IEnumerable<HoChieuHanhKhachView>>(result, "Lấy dánh sách hộ chiếu thành công!"));
            }
            return Ok(new ApiErrorResult<IEnumerable<HoChieuHanhKhachView>>("Danh sách hộ chiếu hành khách trống!"));
        }

        [HttpPut]
        public async Task<IActionResult> Update(HoChieuHanhKhachUpdateParam request)
        {
            try
            {
                if (semaphore.CurrentCount == 0)
                {
                    await semaphore.WaitAsync();
                    return Ok(new ApiErrorResult<string>("Các trường không được để trống!"));
                }

                if (!ModelState.IsValid)
                {
                    return Ok(new ApiErrorResult<string>("Các trường không được để trống!"));
                }
            
                DateTime? ngaySinh = null;
                HoChieuHanhKhach result = new HoChieuHanhKhach();

                if (!string.IsNullOrEmpty(request.NgaySinh))
                {
                    ngaySinh = DateTime.ParseExact(request.NgaySinh, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }

                var user = _userManager.Users.Where(ele => ele.HoChieuDuKhachID == request.DuKhachID).FirstOrDefault();
                if (user != null)
                {
                    user.Email = request.HopThu;
                    user.Fullname = request.HoTen;

                    var isSuccess = await _userManager.UpdateAsync(user);
                    if (isSuccess.Succeeded)
                    {
                        result = await _hoChieuHanhKhachService.Update(new HoChieuHanhKhach
                        {
                            DuKhachID = request.DuKhachID,
                            HoTen = request.HoTen,
                            GioiTinh = request.GioiTinh,
                            HopThu = request.HopThu,
                            NgaySinh = ngaySinh,
                            QuocTichID = request.QuocTichID,
                        });
                    }
                }

                return Ok(new ApiSuccessResult<HoChieuHanhKhach>(result, "Chỉnh sửa hộ chiếu hành khách thành công!"));
            }
            catch (Exception ex)
            {
                return Ok(new ApiErrorResult<HoChieuHanhKhach>($"Lỗi [HoChieuHanhKhachController/Update]: {ex.Message}"));
            }
            finally
            {
                semaphore.Release();
            }
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new ApiErrorResult<string>("Các trường không được để trống!"));
            }

            var user = _userManager.Users.Where(ele => ele.HoChieuDuKhachID == id).FirstOrDefault();
            if (user != null)
            {
                var rs = await _userManager.DeleteAsync(user);
                if (rs.Succeeded)
                {
                    await _hoChieuHanhKhachService.Delete(id);
                }
            }

            return Ok(new ApiSuccessResult<int>("Xoá hộ chiếu hành khách thành công!"));
        }
    }
}
