﻿using Data;
using Data.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Services;
using System.Security.Claims;

namespace WebApp.Api
{
    [Route("/API/[controller]/[action]")]
    [ApiController]
    public class LichTrinhController : ControllerBase
    {
        private readonly ILichTrinhService _lichTrinhService;
        private readonly IThamQuanDiaDiemService _thamQuanDiaDiemService;
        private readonly UserManager<HC_IdentityUser> _userManager;
        public LichTrinhController(ILichTrinhService lichTrinhService
            , IThamQuanDiaDiemService thamQuanDiaDiemService
            , UserManager<HC_IdentityUser> userManager)
        {
            _lichTrinhService = lichTrinhService;
            _thamQuanDiaDiemService = thamQuanDiaDiemService;
            _userManager = userManager;
        }

        [HttpGet]
        public async Task<IActionResult> Gets()
        {
            var result = await _lichTrinhService.Gets();
            if (result != null)
            {
                return Ok(new ApiSuccessResult<IEnumerable<LichTrinh>>(result, "Lấy danh sách lịch trình tham quan địa điểm thành công!"));
            }
            return Ok(new ApiErrorResult<bool>("Danh sách lịch trình tham quan địa điểm trống!"));
        }

        [HttpPost("/API/CheckIn/Gets")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetsByChuongTrinh(int chuongtrinh)
        {
            // Thông tin người dùng đăng nhập
            var userId = User.FindFirstValue(ClaimTypes.Sid);
            var user = await _userManager.FindByIdAsync(userId);

            var lts = await _lichTrinhService.GetsByChuongTrinh(chuongtrinh);
            if (lts != null)
            {
                foreach (var lt in lts)
                {
                    var item = await _thamQuanDiaDiemService.GetsByChuongTrinh(new ThamQuanDiaDiemChuongTrinhModel
                    {
                        ChuongTrinhID = lt.ChuongTrinhID,
                        DuKhachID = user.HoChieuDuKhachID,
                    });

                    lt.IsCheckIn = (item != null) ? true : false;
                }
                return Ok(new ApiSuccessResult<IEnumerable<LichTrinhChuongTrinhModel>>(lts, "Lấy danh sách lịch trình tham quan địa điểm thành công!"));
            }
            return Ok(new ApiErrorResult<bool>("Danh sách lịch trình tham quan địa điểm trống!"));
        }

        [HttpPost]
        public async Task<IActionResult> Create(LichTrinhParam request)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new ApiErrorResult<bool>("Các trường bắt buộc không được để trống!"));
            }

            var result = await _lichTrinhService.Create(new LichTrinh
            {
                ChuongTrinhID = request.ChuongTrinhID,
                DiemThamQuanID = Guid.Parse(request.DiemThamQuanID),
                ThuTu = request.ThuTu,
            });

            return Ok(new ApiSuccessResult<LichTrinh>(result, "Check in thành công!"));
        }
    }
}
