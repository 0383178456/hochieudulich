﻿using Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Services;
using System.Security.Claims;

namespace WebApp.Api
{
    [Route("/API/[controller]/[action]")]
    [ApiController]
    public class NguoiDungController : ControllerBase
    {
        private readonly INguoiDungService _nguoiDungService;
        private readonly IHoChieuHanhKhachService _hoChieuHanhKhachService;
        private readonly IThamQuanDiaDiemService _thamQuanDiaDiemService;
        private readonly IKyQuaySoService _kyQuaySoService;
        private readonly UserManager<HC_IdentityUser> _userManager;
        public NguoiDungController(INguoiDungService nguoiDungService
            , UserManager<HC_IdentityUser> userManager
            , IHoChieuHanhKhachService hoChieuHanhKhachService
            , IThamQuanDiaDiemService thamQuanDiaDiemService
            , IKyQuaySoService kyQuaySoService)
        {
            _nguoiDungService = nguoiDungService;
            _userManager = userManager;
            _hoChieuHanhKhachService = hoChieuHanhKhachService;
            _thamQuanDiaDiemService = thamQuanDiaDiemService;
            _kyQuaySoService = kyQuaySoService;
        }

        [HttpPost]
        public async Task<IActionResult> DangNhap(DangNhapModel request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _nguoiDungService.DangNhap(request);

            if (string.IsNullOrEmpty(result.ResultObj))
                return BadRequest(result);
            
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> DangKy(DangKyModel request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _nguoiDungService.DangKy(request);

            return Ok(result);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> DoiAnhDaiDien(IFormFile file)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _nguoiDungService.ChangeAvatar(file);

            if (result)
                return Ok(new ApiSuccessResult<bool>(result, "Đổi ảnh đại diện thành công!"));
            
            return Ok(new ApiErrorResult<bool>("Đổi ảnh đại diện thất bại"));
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> DoiMatKhau(ChangePasswordParam request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            // Lấy thông tin user đăng nhập
            string userId = User.FindFirstValue(ClaimTypes.Sid);
            var user = await _userManager.FindByIdAsync(userId);

            var result = await _nguoiDungService.ChangePassword(request, user);

            if (result)
                return Ok(new ApiSuccessResult<bool>(result, "Đổi mật khẩu thành công!"));
            
            return Ok(new ApiErrorResult<bool>("Đổi mật khẩu thất bại"));
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> QuenMatKhau(string email, string username)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var find_user = await _userManager.FindByNameAsync(username);
            if (find_user == null)
                return Ok("Không tìm thấy thông tin tài khoản người dùng!");
            

            var find_mail_user = await _userManager.FindByEmailAsync(email);
            if (find_mail_user == null)
                return Ok("Không tìm thấy thông tin email người dùng!");
            

            // Lấy thông tin user đăng nhập
            string userId = User.FindFirstValue(ClaimTypes.Sid);
            var user = await _userManager.FindByIdAsync(userId);

            if (user == null)
                return Ok("Không tìm thấy thông tin người dùng đăng nhập!");
            

            string pass = (new Random().Next(11, 99)).ToString().Trim();
            string mahochieu = $"{DateTime.Now.ToString("yyyyMMddHHmmss").Trim()}{pass}";

            bool isSuccess = await _nguoiDungService.ForgotPassword(new ForgotPasswordParam
            {
                User = user,
                NewPassword = $"HueCIT@{mahochieu}",
            });

            if (isSuccess)
                return Ok("Đổi mật khẩu thành công!");
            
            return Ok("Đổi mật khẩu thất bại!");
        }

        // Lấy thông tin chi tiết của du khách
        [HttpPost("/UserInfo")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Get()
        {
            // Check đăng nhập
            if (!User.Identity.IsAuthenticated)
                return Ok(new ApiErrorResult<bool>("Chưa đăng nhập!"));

            // Thông tin người dùng đăng nhập
            var userId = User.FindFirstValue(ClaimTypes.Sid);
            var user = await _userManager.FindByIdAsync(userId);

            // Thông tin chi tiết của du khách
            var duKhach = await _hoChieuHanhKhachService.GetById(user.HoChieuDuKhachID);
            if (duKhach == null)
                return Ok(new ApiErrorResult<bool>("Không tìm thấy thông tin du khách!"));

            // Thống kê 
            int chuongTrinhThamGia = 0;
            int diaDiemDaCheckIn = 0;
            int thamGiaKyQuay = 0;
            int trungThuong = 0;

            // Danh sách tham quan địa điểm của du khách
            var listTQDD = await _thamQuanDiaDiemService.GetsByDuKhach((Guid)duKhach.DuKhachID);

            // Danh sách tham quan địa điểm của du khách
            var listKQ = await _kyQuaySoService.GetsByDuKhach((Guid)duKhach.DuKhachID);

            if (listTQDD != null)
            {
                // Tổng số chương trình tham gia
                chuongTrinhThamGia = listTQDD.GroupBy(customer => customer.ChuongTrinhID).Count();

                // Tổng số địa điểm đã check in
                diaDiemDaCheckIn = listTQDD.GroupBy(customer => customer.DiemThamQuanID).Count();

                // Tổng số tham gia kỳ quay
                thamGiaKyQuay = listKQ.Count();

                // Tổng số trúng thưởng
                trungThuong = listKQ.Where(e => e.TrungThuong).Count();
            }

            return Ok(new ApiSuccessResult<ThongTinNguoiDung>(new ThongTinNguoiDung
                {
                    HoChieuHanhKhach = duKhach,
                    ChuongTrinhThamGia = chuongTrinhThamGia,
                    DiaDiemDaCheckIn = diaDiemDaCheckIn,
                    ThamGiaKyQuay = thamGiaKyQuay,
                    TrungThuong = trungThuong,
            } ,"Lấy thông tin người dùng thành công!"));
        }
    }
}
