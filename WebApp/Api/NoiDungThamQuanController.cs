﻿using Data.Models;
using Microsoft.AspNetCore.Mvc;
using Services;
using System.Threading.Tasks;

namespace WebApp.Api
{
    [Route("API/[controller]/[action]")]
    [ApiController]
    public class NoiDungThamQuanController : ControllerBase
    {
        private readonly INoiDungThamQuanService _noiDungThamQuanService;
        public NoiDungThamQuanController(INoiDungThamQuanService noiDungThamQuanService)
        {
            _noiDungThamQuanService = noiDungThamQuanService;
        }

        [HttpGet]
        public async Task<IActionResult> Gets()
        {
            var result = await _noiDungThamQuanService.Gets();
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetByID(Guid id)
        {
            var result = await _noiDungThamQuanService.GetByID(id);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetByDiemThamQuan(Guid id)
        {
            var result = await _noiDungThamQuanService.GetByDiemThamQuan(id);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Add(NoiDungThamQuan data)
        {
            var result = await _noiDungThamQuanService.Add(data);
            return Ok(result);
        }

        [HttpPut]
        public async Task<IActionResult> Edit(NoiDungThamQuan data)
        {
            var result = await _noiDungThamQuanService.Edit(data);
            return Ok(result);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteByDiemThamQuanID(Guid id)
        {
            var result = await _noiDungThamQuanService.DeleteByDiemThamQuanID(id);
            return Ok(result);
        }
    }
}
