﻿using Data.Models;
using Microsoft.AspNetCore.Mvc;
using Services;
using System;
using System.Threading.Tasks;

namespace WebApp.Api
{
    [Route("API/[controller]/[action]")]
    [ApiController]
    public class DiemThamQuanController : ControllerBase
    {
        private readonly IDiemThamQuanService _diemThamQuanService;
        public DiemThamQuanController(IDiemThamQuanService diemThamQuanService)
        {
            _diemThamQuanService = diemThamQuanService;
        }

        [HttpPost]
        public async Task<IActionResult> Gets([FromForm]DiemThamQuanSearch data)
        {
            var result = await _diemThamQuanService.Gets(data);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetByID(int id)
        {
            var result = await _diemThamQuanService.GetByID(id);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Add(DiemThamQuanParam data)
        {
            var result = await _diemThamQuanService.Add(data);
            return Ok(result);
        }

        [HttpPut]
        public async Task<IActionResult> Edit(DiemThamQuanParam data)
        {
            var result = await _diemThamQuanService.Edit(data);
            return Ok(result);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _diemThamQuanService.Delete(id);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> ChangeAvataLocation(IFormFile file)
        {
            var result = await _diemThamQuanService.ChangeAvataLocation(file);
            return Ok(result);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteAvataLocation(string fileName)
        {
            var result = await _diemThamQuanService.DeleteAvataLocation(fileName);
            return Ok(result);
        }
    }
}
