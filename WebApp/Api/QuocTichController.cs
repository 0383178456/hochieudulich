﻿using Data;
using Microsoft.AspNetCore.Mvc;
using Services;
using System.Threading.Tasks;

namespace WebApp.Api
{
    [Route("API/[controller]/[action]")]
    [ApiController]
    public class QuocTichController : ControllerBase
    {
        private readonly IQuocTichService _quocTichService;
        public QuocTichController(IQuocTichService quocTichService)
        {
            _quocTichService = quocTichService;
        }

        [HttpGet]
        public async Task<IActionResult> Gets(string? tuKhoa)
        {
            var result = await _quocTichService.Gets(tuKhoa);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetByID(int id)
        {
            var result = await _quocTichService.GetByID(id);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Add(QuocTich data)
        {
            var result = await _quocTichService.Add(data);
            return Ok(result);
        }

        [HttpPut]
        public async Task<IActionResult> Edit(QuocTich data)
        {
            var result = await _quocTichService.Edit(data);
            return Ok(result);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _quocTichService.Delete(id);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CheckName(QuocTichCheck data)
        {
            var result = await _quocTichService.CheckName(data);
            return Ok(result);
        }
    }
}
