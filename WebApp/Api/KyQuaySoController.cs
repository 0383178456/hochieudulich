﻿using Data;
using Data.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Services;
using System.Globalization;
using System.Security.Claims;

namespace WebApp.Api
{
    [Route("/API/[controller]/[action]")]
    [ApiController]
    public class KyQuaySoController : ControllerBase
    {
        private readonly UserManager<HC_IdentityUser> _userManager;
        private readonly IKyQuaySoService _kyQuaySoService;
        private readonly INguoiQuaySoService _nguoiQuaySoService;
        private readonly IThamQuanDiaDiemService _thamQuanDiaDiemService;
        private readonly ILichTrinhService _lichTrinhService;
        public KyQuaySoController(UserManager<HC_IdentityUser> userManager
            , IKyQuaySoService kyQuaySoService
            , INguoiQuaySoService nguoiQuaySoService
            , IThamQuanDiaDiemService thamQuanDiaDiemService
            , ILichTrinhService lichTrinhService)
        {
            _userManager = userManager;
            _kyQuaySoService = kyQuaySoService;
            _nguoiQuaySoService = nguoiQuaySoService;
            _thamQuanDiaDiemService = thamQuanDiaDiemService;
            _lichTrinhService = lichTrinhService;
        }

        [HttpPost("/KyQuaySo")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Create(KyQuaySoParam request)
        {
            if (!ModelState.IsValid)
            {
                return Ok(new ApiErrorResult<bool>("Lỗi validate dữ liệu!"));
            }

            if (!User.Identity.IsAuthenticated)
            {
                return Ok(new ApiErrorResult<bool>("Chưa đăng nhập!"));
            }

            // Convert datetime
            DateTime ngayQuayThuong = DateTime.ParseExact(request.NgayQuayThuong, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime thoiGianTu = DateTime.ParseExact(request.ThoiGianTu, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            DateTime thoiGianDen = DateTime.ParseExact(request.ThoiGianDen, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            // Thông tin người dùng đăng nhập
            var userId = User.FindFirstValue(ClaimTypes.Sid);
            var user = await _userManager.FindByIdAsync(userId);

            // Thông tin danh sách điểm check in của chương trình
            var listLichTrinh = await _lichTrinhService.GetsByChuongTrinh(request.ChuongTrinhID);

            // Thông tin danh sách điểm check in của chương trình của du khách
            var listThamQuanDiaDiem = await _thamQuanDiaDiemService.GetsByChuongTrinh(new ThamQuanDiaDiemChuongTrinhModel
            {
                ChuongTrinhID = request.ChuongTrinhID,
                DuKhachID = user.HoChieuDuKhachID,
            });

            // Kiểm tra người dùng đã check in đủ địa điểm
            if (listLichTrinh.Any() && listThamQuanDiaDiem.Any())
            {
                var data = listLichTrinh.GroupJoin(
                    listThamQuanDiaDiem,
                    dtq => dtq.LichTrinhID,
                    tqdd => tqdd.LichTrinhID,
                    (dtq, tqdd) => new ThamQuanDiaDiemTheoChuongTrinhModel
                    {
                        LichTrinh = dtq,
                        ThamQuanDiaDiem = tqdd.Any() ? tqdd.OrderBy(e => e).MinBy(e => e.ThoiGianThamQuan.Date) : null,
                    }
                );

                bool isCheckInDuDiaDiem = data.Where(e => e.ThamQuanDiaDiem == null).Count() > 0;
                if (!isCheckInDuDiaDiem)
                {
                    // Lấy thông tin điểm tham quan cuối cùng của chương trình
                    var diemThanQuanCuoi = listLichTrinh.MaxBy(e => e.ThuTuDiemThamQuan);

                    // Lấy thông tin điểm tham quan cuối cùng của du khách
                    var thamQuanDiaDiemCuoi = data.Where(e => e.ThamQuanDiaDiem.DiemThamQuanID == diemThanQuanCuoi.DiemThamQuanID).FirstOrDefault();
                    if (DateTime.Compare(thamQuanDiaDiemCuoi.ThamQuanDiaDiem.ThoiGianThamQuan.Date, thoiGianTu.Date) >= 0 && DateTime.Compare(thamQuanDiaDiemCuoi.ThamQuanDiaDiem.ThoiGianThamQuan.Date, thoiGianDen.Date) <= 0)
                    {
                        // Thêm mới kỳ quay số
                        var kyQuaySo = await _kyQuaySoService.Create(new KyQuaySo
                        {
                            TenKyQuay = request.TenKyQuay,
                            NgayQuayThuong = ngayQuayThuong,
                            ThoiGianTu = thoiGianTu,
                            ThoiGianDen = thoiGianDen,
                        });

                        // Thêm mới người quay số theo kỳ quay số
                        if (kyQuaySo != null)
                        {
                            await _nguoiQuaySoService.Create(new NguoiQuaySo
                            {
                                KyQuayID = kyQuaySo.KyQuayID,
                                DuKhachID = Guid.Parse(request.DuKhachID),
                                TrungThuong = false,
                            });

                            return Ok(new ApiSuccessResult<KyQuaySo>(kyQuaySo, "Thêm kỳ quay số thành công!"));
                        }
                        return Ok(new ApiErrorResult<KyQuaySo>("Thêm kỳ quay số thất bại!"));
                    }
                    else
                    {
                        return Ok(new ApiErrorResult<KyQuaySo>("Thời gian check in bạn quá hạn!"));
                    }
                }
                else
                {
                    return Ok(new ApiErrorResult<KyQuaySo>("Bạn chưa đủ điểm check in!"));
                }
            }
            return Ok(new ApiErrorResult<KyQuaySo>("Danh sách tham quan trống!"));
        }
    }
}
