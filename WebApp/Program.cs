﻿using Data;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Services;
using System.Text;
using WebApp.Component;

var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration.GetConnectionString("HC_Connection");
string HueCITAllowSpecificOrigins = "_HueCITAllowSpecificOrigins";

#region SERVICES
// IDENTITY
builder.Services.AddDbContext<HCDbContext>(options => options.UseSqlServer(connectionString));
builder.Services.AddDatabaseDeveloperPageExceptionFilter();
builder.Services.AddIdentity<HC_IdentityUser, IdentityRole>().AddEntityFrameworkStores<HCDbContext>().AddDefaultTokenProviders();

builder.Services.Configure<IdentityOptions>(opts =>
{
    opts.Lockout.AllowedForNewUsers = true;
    opts.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(10);
    opts.Lockout.MaxFailedAccessAttempts = 3;

    // Yêu cầu xác nhận email trước khi đăng nhập
    opts.SignIn.RequireConfirmedEmail = true;
});

// SESSION
builder.Services.AddMvc().AddSessionStateTempDataProvider();
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(30);
});

// DEPENENCE INJECTION
builder.Services.AddControllersWithViews();
builder.Services.AddHttpClient();
builder.Services.AddHttpContextAccessor();
builder.Services.AddSingleton<IActionContextAccessor, ActionContextAccessor>().AddScoped(x => x.GetRequiredService<IUrlHelperFactory>().GetUrlHelper(x.GetRequiredService<IActionContextAccessor>().ActionContext));

builder.Services.AddTransient<IHoChieuHanhKhachService, HoChieuHanhKhachService>();
builder.Services.AddTransient<IQuocTichService, QuocTichService>();
builder.Services.AddTransient<INguoiDungService, NguoiDungService>();
builder.Services.AddTransient<ISendMailService, SendMailService>();
builder.Services.AddTransient<INgonNguService, NgonNguService>();
builder.Services.AddTransient<IChuongTrinhService, ChuongTrinhService>();
builder.Services.AddTransient<IDiemThamQuanService, DiemThamQuanService>();
builder.Services.AddTransient<INoiDungThamQuanService, NoiDungThamQuanService>();
builder.Services.AddTransient<ILichTrinhService, LichTrinhService>();
builder.Services.AddTransient<IThamQuanDiaDiemService, ThamQuanDiaDiemService>();
builder.Services.AddTransient<IStorageService, StorageService>();
builder.Services.AddTransient<ITepKemTheoService, TepKemTheoService>();
builder.Services.AddTransient<INguoiQuaySoService, NguoiQuaySoService>();
builder.Services.AddTransient<IKyQuaySoService, KyQuaySoService>();
#endregion

#region Swagger
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "HueCIT API", Version = "v1" });

    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below. \r\n\r\nExample: 'Bearer 12345abcdef'",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer"
    });

    c.AddSecurityRequirement(new OpenApiSecurityRequirement()
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                },
                Scheme = "oauth2",
                Name = "Bearer",
                In = ParameterLocation.Header,
            },
            new List<string>()
        }
    });
});
#endregion

#region JWT
// LOGIN
builder.Services.ConfigureApplicationCookie(options =>
{
    options.Cookie.HttpOnly = true;
    options.ExpireTimeSpan = TimeSpan.FromMinutes(60);

    options.LoginPath = new PathString("/Login");
    options.LogoutPath = new PathString("/Logout");
    options.AccessDeniedPath = new PathString("/AccessDenied");

    options.SlidingExpiration = true;
});
//
builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
.AddCookie();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
.AddJwtBearer(options =>
{
    options.RequireHttpsMetadata = false;
    options.SaveToken = true;
    options.TokenValidationParameters = new TokenValidationParameters()
    {
        ValidateIssuer = true,
        ValidIssuer = builder.Configuration["Tokens:Issuer"],
        ValidateAudience = true,
        ValidAudience = builder.Configuration["Tokens:Audience"],
        ValidateLifetime = true,
        ValidateIssuerSigningKey = true,
        ClockSkew = System.TimeSpan.Zero,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Tokens:Key"])),
    };
});

builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("HueCITPolicy", policy =>
    {
        policy.AuthenticationSchemes.Add(CookieAuthenticationDefaults.AuthenticationScheme);
        policy.AuthenticationSchemes.Add(JwtBearerDefaults.AuthenticationScheme);
        policy.RequireAuthenticatedUser();
        policy.Build();
    });
});

// CROS
builder.Services.AddCors(options =>
{
    options.AddPolicy(
        name: HueCITAllowSpecificOrigins,
        builder =>
        {
            builder.WithOrigins("https://localhost:7183/", "http://hochieudulich.huecit.com/").AllowAnyHeader().AllowAnyMethod();
        }
    );
});

// IIS
const int maxRequestLimit = 300000000;
builder.Services.Configure<IISServerOptions>(options =>
{
    options.MaxRequestBodySize = maxRequestLimit;
});
#endregion

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseMigrationsEndPoint();
}
else
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.UseSession();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.MapAreaControllerRoute(
    name: "default",
    areaName: "Identity",
    pattern: "{controller=Account}/{action=Login}/{id?}");

app.MapRazorPages();

#region Seeding [Role]
using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;

    var context = services.GetRequiredService<HCDbContext>();
    var config = services.GetRequiredService<IConfiguration>();

    //context.Database.Migrate();

    var userManager = services.GetRequiredService<UserManager<HC_IdentityUser>>();
    var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();

    IdentitySeedData.Initialize(context, userManager, roleManager, config).Wait();
}
#endregion

app.Run();
