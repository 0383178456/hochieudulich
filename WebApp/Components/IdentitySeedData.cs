﻿using Data;
using Microsoft.AspNetCore.Identity;

namespace WebApp.Component
{
    public class IdentitySeedData
    {
        public static async Task Initialize(HCDbContext context,
            UserManager<HC_IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager,
            IConfiguration configuration
        )
        {
            context.Database.EnsureCreated();

            string ADMIN_ID = configuration["UserRoot:UserId"];
            string ROLE_ID = configuration["UserRoot:RoleId"];

            var role = await roleManager.FindByIdAsync(ROLE_ID);
            // ADMIN
            if (role == null)
            {
                IdentityRole quyen = new IdentityRole
                {
                    Id = ROLE_ID,
                    Name = "root"
                };

                await roleManager.CreateAsync(quyen);
            }

            // Quản trị viên
            if (await roleManager.FindByNameAsync("manager") == null)
            {
                IdentityRole quyen = new IdentityRole
                {
                    Name = "manager"
                };

                await roleManager.CreateAsync(quyen);
            }

            // KHÁCH
            if (await roleManager.FindByNameAsync("guest") == null) 
            {
                IdentityRole quyen = new IdentityRole
                {
                    Name = "guest"
                };

                await roleManager.CreateAsync(quyen);
            }

            var root = await userManager.FindByIdAsync(ADMIN_ID);
            if (root == null)
            {
                HC_IdentityUser user = new HC_IdentityUser
                {
                    Id = ADMIN_ID,
                    UserName = "admin",
                    Email = "admin@gmail.com",
                    EmailConfirmed = true,
                    Fullname = "admin",
                };

                var rs = await userManager.CreateAsync(user);

                if (rs.Succeeded)
                {
                    await userManager.AddPasswordAsync(user, "Abcd@123");
                    await userManager.AddToRoleAsync(user, "root");
                }
            }
        }
    }
}
