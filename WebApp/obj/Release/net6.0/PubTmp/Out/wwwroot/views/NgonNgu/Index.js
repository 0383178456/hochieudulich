﻿const url = window.location;
const APIURL = url.protocol + "//" + url.hostname + ":" + url.port;
$(document).ready(function () {

    $("#input-search").keyup(function (event) {
        if (event.keyCode === 13) {
            $('#dataGridLanguages').DataTable().ajax.reload().draw();
        }
    });
    function buildData() {
        let tuKhoa = $('#input-search').val();

        const request = {
            "tuKhoa": tuKhoa,
        };

        return request;
    }
    $("#dataGridLanguages").dataTable({
        "autoWidth": false,
        "ordering": false,
        "bInfo": false,
        "bLengthChange": false,
        "filter": false,
        "language": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Hiện: _MENU_",
            "sZeroRecords": "Không có dữ liệu",
            "sEmptyTable": "Bảng trống",
            "sInfo": "Hiện dòng _START_ đến _END_ trong tổng _TOTAL_ dòng",
            "sInfoEmpty": "Hiện dòng 0 đến 0 trong tổng 0 dòng",
            "sSearch": "Tìm kiếm",
            "sLoadingRecords": "Đang tải...",
            "paginate": {
                next: '<span class="material-icons-outlined">chevron_right</span>',
                previous: '<span class="material-icons-outlined">chevron_left</span>'
            }
        },
        "ajax": {
            url: APIURL + "/API/NgonNgu/Gets",
            type: "GET",
            data: buildData,
            dataSrc: function (data) {
                for (let i = 0; i < data.resultObj.length; i++) {
                    data.resultObj[i].STT = i + 1;
                }
                return data.resultObj;
            },
        },
        "columnDefs": [
            {
                targets: 2,
                render: function (data, type, row, meta) {
                    return '<span data-toggle="tooltip" title="Chỉnh sửa" class="icon edit-command-btn" id=n-"' + meta.row + '"><i class="fa-regular fa-pen-to-square"></i></span><span data-toggle="tooltip" title="Xóa" class="icon delete-command-btn" id=n-"' + meta.row + '"><i class="fa-regular fa-trash-can"></i></span>';
                }
            }
        ],
        "columns": [
            { "data": "STT", "width": "40px", "class": "STT-text center-align" },
            { "data": "tenNgonNgu", "width": "100px", "class": "left-align text-medium text-blue" },
            { "data": "ID", "width": "110px", "class": "center-align group-icon-action" },
        ]

    })

    $("#dataGridLanguages").on('click', '.edit-command-btn', function () {
        var id = $(this).attr("ID").match(/\d+/)[0];
        var data = $('#dataGridLanguages').DataTable().row(id).data();
        $('#idNgonNgu').val(data.ngonNguID);
        $('#tenEdit').val(data.tenNgonNgu);

        $('#modalEdit').modal('show');
    });

    $("#dataGridLanguages").on('click', '.delete-command-btn', function () {
        var id = $(this).attr("ID").match(/\d+/)[0];
        var data = $('#dataGridLanguages').DataTable().row(id).data();
        $('#idNgonNgu').val(data.ngonNguID);
        $('#tenNgonNgu').text(data.tenNgonNgu);

        $('#modalDelete').modal('show');
    })

    $('#btn-search').click(function () {
        $('#dataGridLanguages').DataTable().ajax.reload().draw()
    });
})

function addData() {
    var tenNgonNgu = $('#tenAdd').val();
    if (!checkEmptyBlank(tenNgonNgu)) {
        const check = {
            'TenNgonNgu': tenNgonNgu,
            'Action': 0,
        }
        $.ajax({
            type: 'POST',
            async: false,
            url: APIURL + '/API/NgonNgu/CheckName',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(check),
            success: function (dt) {
                console.log(dt);
                if (dt == 0) {
                    const data = {
                        "TenNgonNgu": tenNgonNgu,
                    }
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: APIURL + '/API/NgonNgu/Add',
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify(data),
                        success: function (data) {
                            $('#modalAdd').modal('hide');
                            closeModal();

                            $('#dataGridLanguages').DataTable().ajax.reload().draw();
                        }
                    })
                } else {
                    alert("Tên ngôn ngữ đã tồn tại!");
                }
            }
        })
    }
}

function editData() {
    var tenNgonNgu = $('#tenEdit').val();
    var idNgonNgu = $('#idNgonNgu').val();
    if (!checkEmptyBlank(tenNgonNgu)) {
        const check = {
            'NgonNguID': idNgonNgu,
            'TenNgonNgu': tenNgonNgu,
            'Action': 1,
        }
        $.ajax({
            type: 'POST',
            async: false,
            url: APIURL + '/API/NgonNgu/CheckName',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(check),
            success: function (dt) {
                if (dt == 0) {
                    const data = {
                        "TenNgonNgu": tenNgonNgu,
                        "NgonNguID": idNgonNgu,
                    }
                    $.ajax({
                        type: "PUT",
                        async: false,
                        url: APIURL + '/API/NgonNgu/Edit',
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify(data),
                        success: function (data) {
                            $('#modalEdit').modal('hide');
                            closeModal();

                            $('#dataGridLanguages').DataTable().ajax.reload().draw();
                        }
                    })
                } else {
                    alert("Tên ngôn ngữ đã tồn tại!");
                }
            }
        })
    }
}

function deleteData() {
    var id = $('#idNgonNgu').val();
    if (id) {
        $.ajax({
            type: 'delete',
            async: false,
            url: APIURL + '/API/NgonNgu/Delete?id=' + id,
            success: function () {
                $('#modalDelete').modal("hide");
                $('#dataGridLanguages').DataTable().ajax.reload().draw()
            },
            error: function (err) {
                console.log("Lỗi: " , err);
            }
        })
    }
}

function closeModal() {
    $("#tenAdd").val("");
}

function checkEmptyBlank(str) {
    if (str == null || str.trim().length === 0) {
        return true
    }
    return false
};