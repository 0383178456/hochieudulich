﻿var url = window.location;
var APIURL = url.protocol + "//" + url.hostname + ":" + url.port;

let isCalled_Edit = false
$(document).ready(function () {
    // Datetimepicker
    $('.dd-mm-yyyy').datetimepicker({
        allowInputToggle: true,
        showClose: false,
        showClear: false,
        showTodayButton: false,
        format: "DD/MM/YYYY",
        locale: 'vi',
        minDate: '1753-01-01',
        maxDate: '9999-12-31',
        useCurrent: false,
    })
        // Quốc tịch
        ; (function () {
            getApiAjaxJson(`/api/QuocTich/Gets`)
                .then((res) => {
                    $('#quoc-tich-model-edit').empty()
                    $('#quoc-tich-model-edit').append(`<option></option>`)
                    for (var i = 0; i < res.resultObj.length; i++) {
                        $('#quoc-tich-model-edit').append(`<option value="${res.resultObj[i].quocTichID}">${res.resultObj[i].tenQuocTich}</option>`)
                    }

                    $('#quoc-tich-model-edit').select2({
                        theme: 'bootstrap4',
                        width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
                        //minimumResultsForSearch: Infinity,
                        placeholder: $(this).data('placeholder'),
                        allowClear: true,
                        dropdownParent: $('.modal-body')
                    });
                })
        })()
    // Datatable
    $('#dataGrid').DataTable({
        "dom": 'Bfrtip',
        "buttons": [
            'excel'
        ],
        "autoWidth": false,
        "ordering": false,
        "bInfo": false,
        "bLengthChange": false,
        "responsive": true,
        "filter": true,
        "scrollY": 700,
        "scrollCollapse": true,
        "paging": false,
        "language": {
            "sProcessing": "Đang xử lý...",
            "sLengthMenu": "Hiện: _MENU_",
            "sZeroRecords": "Không có dữ liệu",
            "sEmptyTable": "Bảng trống",
            "sInfo": "Hiện dòng _START_ đến _END_ trong tổng _TOTAL_ dòng",
            "sInfoEmpty": "Hiện dòng 0 đến 0 trong tổng 0 dòng",
            "sSearch": "Tìm kiếm",
            "sLoadingRecords": "Đang tải...",
            paginate: {
                next: '<i class="material-icons-outlined paging-chevron">chevron_right</i>',
                previous: '<i class="material-icons-outlined paging-chevron">chevron_left</i>'
            }
        },
        "ajax": {
            url: APIURL + '/API/HoChieuHanhKhach/Filter',
            type: 'POST',
            data: dataBuilder(),
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', "Bearer " + tokenJWT);
            },
            dataSrc: function (data) {
                if (data && data.isSuccessed) {
                    for (let i = 0; i < data.resultObj.length; i++) {
                        data.resultObj[i].stt = i + 1;
                    }
                    return data.resultObj;
                }
                return []
            }
        },
        "columnDefs": [
            {
                targets: 2,
                render: function (data, type, row, meta) {
                    if (data == 0) {
                        return 'Nam'
                    } else if (data == 1) {
                        return 'Nữ'
                    } else {
                        return 'Khác'
                    }
                }
            },
            {
                targets: 5,
                render: function (data, type, row, meta) {
                    return formatDatetime(data)
                }
            },
            {
                targets: 7,
                render: function (data, type, row, meta) {
                    return '<span title="Chỉnh sửa" class="material-symbols-outlined edit-icon" id=n-"' + meta.row + '">X</span><span title="Xoá" class="material-symbols-outlined delete-icon" id=n-"' + meta.row + '">V</span>';
                }
            },
        ],
        "columns": [
            { data: "stt", "width": "20px", "class": "center-align" },
            { data: "hoTen", "width": "auto", "class": "center-align" },
            { data: "gioiTinh", "width": "auto", "class": "center-align" },
            { data: "hopThu", "width": "auto", "class": "center-align" },
            { data: "maHoChieu", "width": "auto", "class": "center-align" },
            { data: "ngaySinh", "width": "auto", "class": "center-align" },
            { data: "tenQuocTich", "width": "auto", "class": "center-align" },
            { data: "duKhachID", "class": "center-align nowrap group-icon-action" },
        ],
    });
    // Edit
    $('#dataGrid tbody').on('click', '.edit-icon', function () {
        var id = $(this).attr("ID").match(/\d+/)[0];
        var data = $('#dataGrid').DataTable().row(id).data();

        $('#id_modal_edit').val(data.duKhachID)
        $('#ho-va-ten-model-edit').val(data.hoTen)
        $('#hop-thu-model-edit').val(data.hopThu)
        $('#ngay-sinh-model-edit').val(formatDatetime(data.ngaySinh))
        $('#quoc-tich-model-edit').val(data.quocTichID).trigger('change')
        $(`input[name="gioi-tinh-model-edit"][value="${data.gioiTinh}"]`).prop('checked', true)

        $('#modal_edit').modal('show')
    })
    // Save edit
    $('#btn_save_modal_edit').on('click', async function () {
        let id = $('#id_modal_edit').val()
        let hoTen = $('#ho-va-ten-model-edit').val()
        let hopThu = $('#hop-thu-model-edit').val()
        let ngaySinh = $('#ngay-sinh-model-edit').val()
        let quocTich = $('#quoc-tich-model-edit').val()
        let gioiTinh = $(`input[name="gioi-tinh-model-edit"]:checked`).val()

        let req = {
            "HoTen": hoTen,
            "HopThu": hopThu,
            "NgaySinh": ngaySinh,
            "QuocTichID": Number(quocTich),
            "GioiTinh": Number(gioiTinh),
            "DuKhachID": id,
        }

        let apiEdit = await getApiAjaxAuthorize(`/API/HoChieuHanhKhach/Update`, 'PUT', JSON.stringify(req), tokenJWT)

        alert(apiEdit.message)
        $('#modal_edit').modal('hide')
        $('#dataGrid').DataTable().ajax.reload().draw();
    })
    // Delete
    $('#dataGrid tbody').on('click', '.delete-icon', function () {
        var id = $(this).attr("ID").match(/\d+/)[0];
        var data = $('#dataGrid').DataTable().row(id).data();

        $('#id-modal-delete').val(data.duKhachID)
        $('#modal-delete').modal('show')
    })
    // Save delete
    $('#btn-save-modal-delete').on('click', async function () {
        var duKhachId = $('#id-modal-delete').val()
        let apiDelete = await getApiAjaxAuthorize(`/API/HoChieuHanhKhach/Delete?id=${duKhachId}`, 'DELETE', null, tokenJWT)

        alert(apiDelete.message)
        $('#modal-delete').modal('hide')
        $('#dataGrid').DataTable().ajax.reload().draw();
    })
})

// Request param
function dataBuilder() {
    let hoten = $('#tuKhoa').val()
    let req = {
        "HoTen": hoten ? hoten : null,
    }

    return req
}