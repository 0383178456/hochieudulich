﻿using Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Services;
using System.Data;
using System.Security.Claims;
using WebApp.Common;

namespace WebApp.Controllers
{
    [Authorize(Roles = "root")]
    public class LanguagesController : Controller
    {
        private readonly ILogger<LanguagesController> _logger;
        private readonly IHoChieuHanhKhachService _hoChieuHanhKhachService;
        private readonly IConfiguration _configuration;
        private readonly UserManager<HC_IdentityUser> _userManager;

        public LanguagesController(ILogger<LanguagesController> logger
           , IHoChieuHanhKhachService hoChieuHanhKhachService
           , IConfiguration configuration
           , UserManager<HC_IdentityUser> userManager)
        {
            _logger = logger;
            _hoChieuHanhKhachService = hoChieuHanhKhachService;
            _configuration = configuration;
            _userManager = userManager;
        }
        public async Task<IActionResult> Languages()
        {
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var roles = await _userManager.GetRolesAsync(user);
                ViewBag.TokenJWT = JwtCommon.GetToken(_configuration, user, roles);
            }

            return View();
        }
    }
}
