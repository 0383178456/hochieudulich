﻿using Dapper;
using Data;
using Microsoft.Data.SqlClient;
using System.Data;

namespace Services
{
    public interface IQuocTichService
    {
        Task<ApiResult<IEnumerable<QuocTich>>> Gets(string? tuKhoa);
        Task<ApiResult<QuocTich>> GetByID(int id);
        Task<QuocTich> Add(QuocTich data);
        Task<QuocTich> Edit(QuocTich data);
        Task<int> Delete(int id);
        Task<int> CheckName(QuocTichCheck data);
    }
    public class QuocTichService : BaseService, IQuocTichService
    {
        public async Task<QuocTich> Add(QuocTich data)
        {
            using(SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@TenQuocTich", data.TenQuocTich);

                    QuocTich? list = conn.QueryFirstOrDefault<QuocTich>("SP_HC_QuocTich_Add", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                } catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<QuocTich> Edit(QuocTich data)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@QuocTichID", data.QuocTichID);
                    parameters.Add("@TenQuocTich", data.TenQuocTich);

                    QuocTich? list = conn.QueryFirstOrDefault<QuocTich>("SP_HC_QuocTich_Edit", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
            throw new NotImplementedException();
        }

        public async Task<ApiResult<IEnumerable<QuocTich>>> Gets(string? tuKhoa)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@TuKhoa", tuKhoa);

                    IEnumerable<QuocTich>? list = await conn.QueryAsync<QuocTich>("SP_HC_QuocTich_Gets", parameters, commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<IEnumerable<QuocTich>>(list, "Lấy thông tin danh sách ngôn ngữ thành công!");
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<IEnumerable<QuocTich>>("Có lỗi xảy ra: " + ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<ApiResult<QuocTich>> GetByID(int id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@QuocTichID", id);

                    QuocTich list = await conn.QueryFirstOrDefaultAsync<QuocTich>("SP_HC_QuocTich_GetByID", parameters, commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<QuocTich>(list, "Lấy thông tin ngôn ngữ thành công!");
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<QuocTich>("Có lỗi xảy ra: " + ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<int> Delete(int id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@QuocTichID", id);

                    int list = conn.QueryFirstOrDefault<int>("SP_HC_QuocTich_Delete", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<int> CheckName(QuocTichCheck data)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@QuocTichID", data.QuocTichID);
                    parameters.Add("@TenQuocTich", data.TenQuocTich);
                    parameters.Add("@Action", data.Action);

                    int list = conn.QueryFirstOrDefault<int>("SP_HC_QuocTich_Check", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
    }
}
