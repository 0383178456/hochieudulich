﻿using Data.Models;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Data.SqlClient;
using System.Data;
using Microsoft.AspNetCore.Http;

namespace Services
{
    public interface IDiemThamQuanService
    {
        Task<ApiResult<IEnumerable<DiemThamQuan>>> Gets(DiemThamQuanSearch data);
        Task<ApiResult<DiemThamQuan>> GetByID(int id);
        Task<DiemThamQuanParam> Add(DiemThamQuanParam data);
        Task<DiemThamQuanParam> Edit(DiemThamQuanParam data);
        Task<int> Delete(Guid id);
        Task<bool> ChangeAvataLocation(IFormFile file);
        Task<bool> DeleteAvataLocation(string filenameOld);
    }
    public class DiemThamQuanService : BaseService, IDiemThamQuanService
    {
        private readonly IStorageService _storageService;
        public DiemThamQuanService(IStorageService storageService)
        {
            _storageService = storageService;
        }

        public async Task<ApiResult<IEnumerable<DiemThamQuan>>> Gets(DiemThamQuanSearch data)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@TuKhoa", data.TuKhoa);
                    parameters.Add("@NgonNguID", data.NgonNguID);

                    IEnumerable<DiemThamQuan> list = await conn.QueryAsync<DiemThamQuan>("SP_HC_DiemThamQuan_Gets", parameters,commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<IEnumerable<DiemThamQuan>>(list, "Lấy thông tin danh sách điểm tham quan thành công!");
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<IEnumerable<DiemThamQuan>>("Có lỗi xảy ra: " + ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        public async Task<ApiResult<DiemThamQuan>> GetByID(int id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@ChuongTrinhID", id);

                    DiemThamQuan list = await conn.QueryFirstOrDefaultAsync<DiemThamQuan>("SP_HC_DiemThamQuan_Gets", parameters, commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<DiemThamQuan>(list, "Lấy thông tin danh sách điểm tham quan thành công!");
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<DiemThamQuan>("Có lỗi xảy ra: " + ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        public async Task<DiemThamQuanParam> Add(DiemThamQuanParam data)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@KinhDo", data.KinhDo);
                    parameters.Add("@ViDo", data.ViDo);
                    parameters.Add("@BanKinhQuyUoc", data.BanKinhQuyUoc);
                    parameters.Add("@ThuTu", data.ThuTu);
                    parameters.Add("@AnhDaiDien", data.AnhDaiDien);

                    DiemThamQuanParam? list = conn.QueryFirstOrDefault<DiemThamQuanParam>("SP_HC_DiemThamQuan_Add", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        public async Task<DiemThamQuanParam> Edit(DiemThamQuanParam data)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@KinhDo", data.KinhDo);
                    parameters.Add("@ViDo", data.ViDo);
                    parameters.Add("@BanKinhQuyUoc", data.BanKinhQuyUoc);
                    parameters.Add("@ThuTu", data.ThuTu);
                    parameters.Add("@AnhDaiDien", data.AnhDaiDien);
                    parameters.Add("@DiemThamQuanID", data.DiemThamQuanID);

                    DiemThamQuanParam? list = conn.QueryFirstOrDefault<DiemThamQuanParam>("SP_HC_DiemThamQuan_Edit", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        public async Task<int> Delete(Guid id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@DiemThamQuanID", id);

                    int list = conn.QueryFirstOrDefault<int>("SP_HC_DiemThamQuan_Delete", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<bool> ChangeAvataLocation(IFormFile file)
        {
            //string time = DateTime.Now.ToString("ddMMyyyyHHmmss");
            string _filename = Path.GetFileName(file.FileName);

            await _storageService.SaveFileAsync(file.OpenReadStream(), "AvatarLocation", _filename);

            return true;
        }

        public async Task<bool> DeleteAvataLocation(string filenameOld)
        {
            await _storageService.DeleteFileAsync("AvatarLocation", filenameOld);
            return true;
        }
    }
}
