﻿using Dapper;
using Data.Models;
using Microsoft.Data.SqlClient;
using Org.BouncyCastle.Asn1.Ocsp;
using System.Collections.Generic;
using System.Data;

namespace Services
{
    public interface IKyQuaySoService
    {
        Task<KyQuaySo> Create(KyQuaySo request); 
        Task<IEnumerable<KyQuaySoInfo>> GetsByDuKhach(Guid dukhach);
    }

    public class KyQuaySoService : BaseService, IKyQuaySoService
    {
        public async Task<KyQuaySo> Create(KyQuaySo request)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@TenKyQuay", request.TenKyQuay);
                    parameters.Add("@NgayQuayThuong", request.NgayQuayThuong);
                    parameters.Add("@ThoiGianTu", request.ThoiGianTu);
                    parameters.Add("@ThoiGianDen", request.ThoiGianDen);

                    KyQuaySo? list = await conn.QueryFirstOrDefaultAsync<KyQuaySo>("SP_HC_KyQuaySo_Add", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<IEnumerable<KyQuaySoInfo>> GetsByDuKhach(Guid dukhach)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@DuKhachID", dukhach);

                    IEnumerable<KyQuaySoInfo> list = await conn.QueryAsync<KyQuaySoInfo>("SP_HC_KyQuaySo_GetsByDuKhach", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
    }
}
