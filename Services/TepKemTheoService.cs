﻿using Dapper;
using Data;
using Data.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface ITepKemTheoService
    {
        Task<ApiResult<TepKemTheoParam>> Add(TepKemTheoParam data);
        Task<ApiResult<TepKemTheo>> Edit(TepKemTheo data);
        Task<ApiResult<int>> Delete(Guid id);
        Task<ApiResult<TepKemTheo>> GetByChuTheID(Guid id);

    }
    public class TepKemTheoService : BaseService, ITepKemTheoService
    {
        public async Task<ApiResult<TepKemTheoParam>> Add(TepKemTheoParam data)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@ChuTheID", data.ChuTheID);
                    parameters.Add("@MoTa", data.MoTa);
                    parameters.Add("@LienKet", data.LienKet);

                    TepKemTheoParam list = conn.QueryFirstOrDefault<TepKemTheoParam>("SP_HC_TepKemTheo_Add", parameters, commandType: CommandType.StoredProcedure);

                    //
                    TepKemTheoParam result = new TepKemTheoParam
                    {
                        ChuTheID = list.ChuTheID,
                        MoTa = list.MoTa,
                        LienKet = data.LienKet,
                    };

                    return new ApiSuccessResult<TepKemTheoParam>(result, "Thêm mới tệp kèm theo thành công");
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<TepKemTheoParam>(ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<ApiResult<int>> Delete(Guid id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@ChuTheID", id);
                    int list = conn.QueryFirstOrDefault<int>("SP_HC_TepKemTheo_Delete_ChuTheID", parameters, commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<int>(list, "Xóa danh sách tệp kèm theo thành công");
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<int>(ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<ApiResult<TepKemTheo>> Edit(TepKemTheo data)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@TepID", data.TepID);
                    parameters.Add("@MoTa", data.MoTa);
                    parameters.Add("@LienKet", data.LienKet);

                    TepKemTheo list = conn.QueryFirstOrDefault<TepKemTheo>("SP_HC_TepKemTheo_Edit", parameters, commandType: CommandType.StoredProcedure);

                    //
                    TepKemTheo result = new TepKemTheo
                    {
                        TepID = list.TepID,
                        MoTa = list.MoTa,
                        LienKet = data.LienKet,
                    };

                    return new ApiSuccessResult<TepKemTheo>(result, "Cập nhật tệp kèm theo thành công");
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<TepKemTheo>(ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<ApiResult<TepKemTheo>> GetByChuTheID(Guid id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@ChuTheID", id);

                    TepKemTheo list = await conn.QueryFirstOrDefaultAsync<TepKemTheo>("SP_HC_TepKemTheo_Gets", parameters, commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<TepKemTheo>(list, "Lấy thông tin danh sách tệp kèm theo thành công!");
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<TepKemTheo>("Có lỗi xảy ra: " + ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
    }
}
