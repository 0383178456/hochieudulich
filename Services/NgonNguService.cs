﻿using Dapper;
using Data;
using Microsoft.Data.SqlClient;
using System.Data;

namespace Services
{
    public interface INgonNguService
    {
        Task<ApiResult<IEnumerable<NgonNgu>>> Gets(string? tuKhoa);
        Task<ApiResult<NgonNgu>> GetByID(int id);
        Task<NgonNgu> Add(NgonNgu data);
        Task<NgonNgu> Edit(NgonNgu data);
        Task<int> Delete(int id);
        Task<int> CheckName(NgonNguCheck data);
        Task<ApiResult<IEnumerable<NgonNgu>>> GetByDiemThamQuanNotExist(Guid id);
        Task<ApiResult<IEnumerable<NgonNgu>>> GetByDiemThamQuanDoExist(Guid id);
    }
    public class NgonNguService : BaseService, INgonNguService
    {
        public async Task<ApiResult<IEnumerable<NgonNgu>>> Gets(string? tuKhoa)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@Search", tuKhoa);

                    IEnumerable<NgonNgu>? list = await conn.QueryAsync<NgonNgu>("SP_HC_NgonNgu_Gets", parameters,commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<IEnumerable<NgonNgu>>(list, "Lấy thông tin danh sách ngôn ngữ thành công!");
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<IEnumerable<NgonNgu>>("Có lỗi xảy ra: " + ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<NgonNgu> Add(NgonNgu data)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@TenNgonNgu", data.TenNgonNgu);

                    NgonNgu? list = conn.QueryFirstOrDefault<NgonNgu>("SP_HC_NgonNgu_Add", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<NgonNgu> Edit(NgonNgu data)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@TenNgonNgu", data.TenNgonNgu);
                    parameters.Add("@NgonNguID", data.NgonNguID);

                    NgonNgu? list = conn.QueryFirstOrDefault<NgonNgu>("SP_HC_NgonNgu_Edit", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<int> Delete(int id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@NgonNguID", id);

                    int list = conn.QueryFirstOrDefault<int>("SP_HC_NgonNgu_Delete", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<ApiResult<NgonNgu>> GetByID(int id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@NgonNguID", id);

                    NgonNgu list = await conn.QueryFirstOrDefaultAsync<NgonNgu>("SP_HC_NgonNgu_GetByID", parameters, commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<NgonNgu>(list, "Lấy thông tin ngôn ngữ thành công!");
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<NgonNgu>("Có lỗi xảy ra: " + ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<int> CheckName(NgonNguCheck data)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@TenNgonNgu", data.TenNgonNgu);
                    parameters.Add("@NgonNguID", data.NgonNguID);
                    parameters.Add("@Action", data.Action);

                    int list = conn.QueryFirstOrDefault<int>("SP_HC_NgonNgu_Check", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<ApiResult<IEnumerable<NgonNgu>>> GetByDiemThamQuanNotExist(Guid id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@DiemThamQuanID", id);

                    IEnumerable<NgonNgu>? list = await conn.QueryAsync<NgonNgu>("SP_HC_NgonNgu_GetByDiemThamQuan_NotExist", parameters, commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<IEnumerable<NgonNgu>>(list, "Lấy thông tin danh sách ngôn ngữ còn lại thành công!");
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<IEnumerable<NgonNgu>>("Có lỗi xảy ra: " + ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<ApiResult<IEnumerable<NgonNgu>>> GetByDiemThamQuanDoExist(Guid id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@DiemThamQuanID", id);

                    IEnumerable<NgonNgu>? list = await conn.QueryAsync<NgonNgu>("SP_HC_NgonNgu_GetByDiemThamQuan", parameters, commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<IEnumerable<NgonNgu>>(list, "Lấy thông tin danh sách ngôn ngữ còn lại thành công!");
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<IEnumerable<NgonNgu>>("Có lỗi xảy ra: " + ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
    }
}
