﻿using Dapper;
using Data.Models;
using Microsoft.Data.SqlClient;
using System.Data;

namespace Services
{
    public interface ILichTrinhService
    {
        Task<IEnumerable<LichTrinh>> Gets();
        Task<IEnumerable<LichTrinhChuongTrinhModel>> GetsByChuongTrinh(int id);
        Task<LichTrinh> Get(int chuongTrinhID, Guid diemThamQuanID);
        Task<LichTrinh> Create(LichTrinh request); 
    }

    public class LichTrinhService : BaseService, ILichTrinhService
    {
        public async Task<LichTrinh> Create(LichTrinh request)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@DiemThamQuanID", request.DiemThamQuanID);
                    parameters.Add("@ChuongTrinhID", request.ChuongTrinhID);
                    parameters.Add("@ThuTu", request.ThuTu);

                    LichTrinh list = await conn.QueryFirstOrDefaultAsync<LichTrinh>("SP_HC_LichTrinh_Add", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        public async Task<IEnumerable<LichTrinh>> Gets()
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();

                    IEnumerable<LichTrinh> list = await conn.QueryAsync<LichTrinh>("SP_HC_HoChieuDuKhach_Gets", commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch(Exception ex)
                {
                    return null;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        public async Task<IEnumerable<LichTrinhChuongTrinhModel>> GetsByChuongTrinh(int id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@ChuongTrinhID", id);

                    IEnumerable<LichTrinhChuongTrinhModel> list = await conn.QueryAsync<LichTrinhChuongTrinhModel>("SP_HC_LichTrinh_GetsByChuongTrinh", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        public async Task<LichTrinh> Get(int chuongTrinhID, Guid diemThamQuanID)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@ChuongTrinhID", chuongTrinhID);
                    parameters.Add("@DiemThamQuanID", diemThamQuanID);

                    LichTrinh list = await conn.QueryFirstOrDefaultAsync<LichTrinh>("SP_HC_LichTrinh_GetsByChuongTrinhAndDiemThamQuan", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
    }
}
