﻿using Dapper;
using Data.Models;
using Microsoft.Data.SqlClient;
using Org.BouncyCastle.Asn1.Ocsp;
using System.Data;

namespace Services
{
    public interface IThamQuanDiaDiemService
    {
        Task<IEnumerable<ThamQuanDiaDiem>> Gets();
        Task<IEnumerable<ThamQuanDiaDiem>> GetsByDuKhach(Guid dukhach);
        Task<ThamQuanDiaDiem> GetsByChuongTrinhAndDiemThamQuan(ThamQuanDiaDiemChuongTrinhDiemThamQuanModel request);
        Task<IEnumerable<ThamQuanDiaDiem>> GetsByChuongTrinh(ThamQuanDiaDiemChuongTrinhModel request);
        Task<ThamQuanDiaDiem> Create(ThamQuanDiaDiem request);
    }

    public class ThamQuanDiaDiemService : BaseService, IThamQuanDiaDiemService
    {
        public async Task<ThamQuanDiaDiem> Create(ThamQuanDiaDiem request)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@DuKhachID", request.DuKhachID);
                    parameters.Add("@LichTrinhID", request.LichTrinhID);

                    ThamQuanDiaDiem list = await conn.QueryFirstOrDefaultAsync<ThamQuanDiaDiem>("SP_HC_ThamQuanDiaDiem_Add", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        public async Task<IEnumerable<ThamQuanDiaDiem>> Gets()
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();

                    IEnumerable<ThamQuanDiaDiem> list = await conn.QueryAsync<ThamQuanDiaDiem>("SP_HC_ThamQuanDiaDiem_Gets", commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch(Exception ex)
                {
                    return null;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        public async Task<ThamQuanDiaDiem> GetsByChuongTrinhAndDiemThamQuan(ThamQuanDiaDiemChuongTrinhDiemThamQuanModel request)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@DuKhachID", request.DuKhachID);
                    parameters.Add("@ChuongTrinhID", request.ChuongTrinhID);
                    parameters.Add("@DiemThamQuanID", request.DiemThamQuanID);

                    ThamQuanDiaDiem list = await conn.QueryFirstOrDefaultAsync<ThamQuanDiaDiem>("SP_HC_ThamQuanDiaDiem_GetsByChuongTrinhAndDuKhach", parameters, commandType: CommandType.StoredProcedure);
                    if (list != null)
                    {
                        return list;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        public async Task<IEnumerable<ThamQuanDiaDiem>> GetsByChuongTrinh(ThamQuanDiaDiemChuongTrinhModel request)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@DuKhachID", request.DuKhachID);
                    parameters.Add("@ChuongTrinhID", request.ChuongTrinhID);

                    IEnumerable<ThamQuanDiaDiem> list = await conn.QueryAsync<ThamQuanDiaDiem>("SP_HC_ThamQuanDiaDiem_GetsByChuongTrinh", parameters, commandType: CommandType.StoredProcedure);
                    if (list.Any())
                    {
                        return list;
                    }
                    return null;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        public async Task<IEnumerable<ThamQuanDiaDiem>> GetsByDuKhach(Guid dukhach)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@DuKhachID", dukhach);

                    IEnumerable<ThamQuanDiaDiem> list = await conn.QueryAsync<ThamQuanDiaDiem>("SP_HC_ThamQuanDiaDiem_GetsByDuKhach", parameters, commandType: CommandType.StoredProcedure);
                    if (list.Any())
                        return list;
                    return null;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
    }
}
