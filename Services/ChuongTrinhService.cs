﻿using Dapper;
using Data;
using Microsoft.Data.SqlClient;
using System.Data;

namespace Services
{
    public interface IChuongTrinhService
    {
        Task<ApiResult<IEnumerable<ChuongTrinh>>> Gets();
        Task<ApiResult<ChuongTrinh>> GetByID(int id);
        Task<ChuongTrinh> Add(ChuongTrinh data);
        Task<ChuongTrinh> Edit(ChuongTrinh data);
        Task<int> Delete(int id);
    }
    public class ChuongTrinhService : BaseService, IChuongTrinhService
    {
        public async Task<ApiResult<IEnumerable<ChuongTrinh>>> Gets()
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();

                    IEnumerable<ChuongTrinh> list = await conn.QueryAsync<ChuongTrinh>("SP_HC_ChuongTrinh_Gets", commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<IEnumerable<ChuongTrinh>>(list, "Lấy thông tin danh sách chương trình thành công!");
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<IEnumerable<ChuongTrinh>>("Có lỗi xảy ra: " + ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        public async Task<ApiResult<ChuongTrinh>> GetByID(int id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@ChuongTrinhID", id);

                    ChuongTrinh list = await conn.QueryFirstOrDefaultAsync<ChuongTrinh>("SP_HC_ChuongTrinh_Gets",parameters, commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<ChuongTrinh>(list, "Lấy thông tin danh sách chương trình thành công!");
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<ChuongTrinh>("Có lỗi xảy ra: " + ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        public async Task<ChuongTrinh> Add(ChuongTrinh data)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@TenChuongTrinh", data.TenChuongTrinh);
                    parameters.Add("@GhiChu", data.GhiChu);

                    ChuongTrinh? list = conn.QueryFirstOrDefault<ChuongTrinh>("SP_HC_ChuongTrinh_Add", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        public async Task<ChuongTrinh> Edit(ChuongTrinh data)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@ChuongTrinhID", data.ChuongTrinhID);
                    parameters.Add("@TenChuongTrinh", data.TenChuongTrinh);
                    parameters.Add("@GhiChu", data.GhiChu);

                    ChuongTrinh? list = conn.QueryFirstOrDefault<ChuongTrinh>("SP_HC_ChuongTrinh_Edit", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        public async Task<int> Delete(int id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@ChuongTrinhID", id);

                    int list = conn.QueryFirstOrDefault<int>("SP_HC_ChuongTrinh_Delete", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
    }
}
