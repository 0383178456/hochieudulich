﻿using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using MimeKit;
using MimeKit.Text;

namespace Services
{
    public interface ISendMailService
    {
        Task SendMail(string toEmailAddress, string subject, string content);
    }

    public class SendMailService : BaseService, ISendMailService
    {
        private readonly IConfiguration _configuration;
        public SendMailService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task SendMail(string toEmailAddress, string subject, string content)
        {
            var emailHost = _configuration["SmtpEmail:email"];
            var pwd = _configuration["SmtpEmail:pwd"];
            var host = _configuration["SmtpEmail:host"];
            var port = _configuration["SmtpEmail:port"];

            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse(emailHost);
            email.To.Add(MailboxAddress.Parse(toEmailAddress));
            email.Subject = subject;

            var builder = new BodyBuilder();
            email.Body = new TextPart(TextFormat.Html) { Text = content };

            using (var smtp = new SmtpClient())
            {
                await smtp.ConnectAsync(host, Convert.ToInt32(port), MailKit.Security.SecureSocketOptions.StartTls);
                await smtp.AuthenticateAsync(emailHost, pwd);

                await smtp.SendAsync(email);

                smtp.Disconnect(true);
            }
        }
    }
}
