﻿using Dapper;
using Data;
using Microsoft.Data.SqlClient;
using System.Data;

namespace Services
{
    public interface IHoChieuHanhKhachService
    {
        Task<IEnumerable<HoChieuHanhKhachView>> Gets(); 
        Task<HoChieuHanhKhach> Create(HoChieuHanhKhach request);
        Task<HoChieuHanhKhach> Update(HoChieuHanhKhach request);
        Task<int> Delete(Guid id);
        Task<IEnumerable<HoChieuHanhKhachView>> Filter(HoChieuHanhKhachFilter filter);
        Task<HoChieuHanhKhachView> GetById(Guid id);
    }

    public class HoChieuHanhKhachService : BaseService, IHoChieuHanhKhachService
    {
        private readonly IQuocTichService _quocTichService;
        public HoChieuHanhKhachService(IQuocTichService quocTichService)
        {
            _quocTichService = quocTichService;
        }
        public async Task<HoChieuHanhKhach> Create(HoChieuHanhKhach request)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@MaHoChieu", request.MaHoChieu);
                    parameters.Add("@HoTen", request.HoTen);
                    parameters.Add("@GioiTinh", request.GioiTinh);
                    parameters.Add("@HopThu", request.HopThu);
                    parameters.Add("@NgaySinh", request.NgaySinh);
                    parameters.Add("@QuocTichID", request.QuocTichID);

                    HoChieuHanhKhach list = await conn.QueryFirstOrDefaultAsync<HoChieuHanhKhach>("SP_HC_HoChieuDuKhach_Add", parameters, commandType: CommandType.StoredProcedure);
                    if (list != null && list.QuocTichID != null)
                    {
                        var quoctich = await _quocTichService.GetByID((int)list.QuocTichID);
                        if (quoctich.IsSuccessed) 
                        {
                            list.TenQuocTich = quoctich.ResultObj.TenQuocTich;
                        }
                    }

                    return list;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<int> Delete(Guid id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();

                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@DuKhachID", id);

                    int list = await conn.QueryFirstOrDefaultAsync<int>("SP_HC_HoChieuDuKhach_Delete", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    return -1;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<IEnumerable<HoChieuHanhKhachView>> Gets()
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();

                    IEnumerable<HoChieuHanhKhachView> list = await conn.QueryAsync<HoChieuHanhKhachView>("SP_HC_HoChieuDuKhach_Gets", commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch(Exception ex)
                {
                    return null;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<IEnumerable<HoChieuHanhKhachView>> Filter(HoChieuHanhKhachFilter filter)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@HoTen", filter.HoTen);

                    IEnumerable<HoChieuHanhKhachView> list = await conn.QueryAsync<HoChieuHanhKhachView>("SP_HC_HoChieuDuKhach_Filter", commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<HoChieuHanhKhach> Update(HoChieuHanhKhach request)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();

                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@DuKhachID", request.DuKhachID);
                    parameters.Add("@HoTen", request.HoTen);
                    parameters.Add("@GioiTinh", request.GioiTinh);
                    parameters.Add("@HopThu", request.HopThu);
                    parameters.Add("@NgaySinh", request.NgaySinh);
                    parameters.Add("@QuocTichID", request.QuocTichID);

                    HoChieuHanhKhach list = await conn.QueryFirstOrDefaultAsync<HoChieuHanhKhach>("SP_HC_HoChieuDuKhach_Edit", parameters, commandType: CommandType.StoredProcedure);
                    
                    return list;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<HoChieuHanhKhachView> GetById(Guid id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@DuKhachID", id);

                    HoChieuHanhKhachView list = await conn.QueryFirstOrDefaultAsync<HoChieuHanhKhachView>("SP_HC_HoChieuDuKhach_GetById", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
    }
}
