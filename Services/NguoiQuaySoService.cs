﻿using Dapper;
using Data.Models;
using Microsoft.Data.SqlClient;
using System.Data;

namespace Services
{
    public interface INguoiQuaySoService
    {
        Task<NguoiQuaySo?> Create(NguoiQuaySo request); 
    }

    public class NguoiQuaySoService : BaseService, INguoiQuaySoService
    {
        public async Task<NguoiQuaySo?> Create(NguoiQuaySo request)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@KyQuayID", request.KyQuayID);
                    parameters.Add("@DuKhachID", request.DuKhachID);
                    parameters.Add("@TrungThuong", request.TrungThuong);

                    NguoiQuaySo? list = await conn.QueryFirstOrDefaultAsync<NguoiQuaySo>("SP_HC_NguoiQuaySo_Add", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
    }
}
