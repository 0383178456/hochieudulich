﻿using Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using WebApp.Common;

namespace Services
{
    public interface INguoiDungService
    {
        Task<ApiResult<string>> DangNhap(DangNhapModel request);
        Task<ApiResult<HoChieuHanhKhachView>> DangKy(DangKyModel request);
        Task<bool> ChangeAvatar(IFormFile file);
        Task<bool> ChangePassword(ChangePasswordParam request, HC_IdentityUser user);
        Task<bool> ForgotPassword(ForgotPasswordParam request);
    }

    public class NguoiDungService : BaseService, INguoiDungService
    {
        private readonly UserManager<HC_IdentityUser> _userManager;
        private readonly SignInManager<HC_IdentityUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IConfiguration _config;
        private readonly IHoChieuHanhKhachService _hoChieuHanhKhachService;
        private readonly ISendMailService _sendMailService;
        private readonly IUrlHelperFactory _urlHelperFactory;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IActionContextAccessor _actionContextAccessor;
        private readonly IStorageService _storageService;

        public NguoiDungService(UserManager<HC_IdentityUser> userManager,
           SignInManager<HC_IdentityUser> signInManager,
           RoleManager<IdentityRole> roleManager,
           IConfiguration config,
           IHoChieuHanhKhachService hoChieuHanhKhachService,
           ISendMailService sendMailService,
           IUrlHelperFactory urlHelperFactory,
           IHttpContextAccessor httpContextAccessor,
           IActionContextAccessor actionContextAccessor,
           IStorageService storageService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _config = config;
            _hoChieuHanhKhachService = hoChieuHanhKhachService;
            _urlHelperFactory = urlHelperFactory;
            _httpContextAccessor = httpContextAccessor;
            _actionContextAccessor = actionContextAccessor;
            _sendMailService = sendMailService;
            _storageService = storageService;
        }

        public async Task<ApiResult<string>> DangNhap(DangNhapModel request)
        {
            var user = await _userManager.FindByNameAsync(request.TaiKhoan);
            if (user == null) return new ApiErrorResult<string>("Tài khoản hoặc mật khẩu không đúng!");

            var result = await _signInManager.PasswordSignInAsync(user, request.MatKhau, request.GhiNho, true);
            if (!result.Succeeded)
            {
                return new ApiErrorResult<string>("Tài khoản hoặc mật khẩu không đúng!");
            }

            var roles = await _userManager.GetRolesAsync(user);
            var tokenString = JwtCommon.GetToken(_config, user, roles);

            return new ApiSuccessResult<string>(tokenString, "Xác thực thành công!");
        }

        public async Task<ApiResult<HoChieuHanhKhachView>> DangKy(DangKyModel request)
        {
            string code = (new Random().Next(11, 99)).ToString().Trim();
            string mahochieu = $"{DateTime.Now.ToString("yyyyMMddHHmmss").Trim()}{code}";
            var user = await _userManager.FindByNameAsync(mahochieu);
            string pass = $"HueCIT@{mahochieu}";
            var email = await _userManager.FindByEmailAsync(request.HopThu);

            // Validate
            if (user != null)
            {
                return new ApiErrorResult<HoChieuHanhKhachView>("Tài khoản đã tồn tại");
            }

            if (email != null)
            {
                return new ApiErrorResult<HoChieuHanhKhachView>("Emai đã tồn tại");
            }
            
            // Create User
            user = new HC_IdentityUser()
            {
                UserName = mahochieu.Trim(),
                Email = request.HopThu,
                PhoneNumber = request.SoDienThoai,
                Fullname = request.HoTen,
            };
            var result = await _userManager.CreateAsync(user, pass);

            // Create Hộ chiếu hành khách
            if (result.Succeeded)
            {
                DateTime? ngaySinh = null;

                if (!string.IsNullOrEmpty(request.NgaySinh))
                {
                    ngaySinh = DateTime.ParseExact(request.NgaySinh, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }

                var apiHoChieu = await _hoChieuHanhKhachService.Create(new HoChieuHanhKhach
                {
                    MaHoChieu = mahochieu,
                    GioiTinh = request.GioiTinh,
                    HopThu = request.HopThu,
                    HoTen = request.HoTen,
                    NgaySinh = ngaySinh,
                    QuocTichID = request.QuocTich
                });

                if (apiHoChieu != null)
                {
                    user.HoChieuDuKhachID = apiHoChieu.DuKhachID;

                    var resultUpdate = await _userManager.UpdateAsync(user);
                    if (resultUpdate.Succeeded)
                    {
                        var codeToken = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        codeToken = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(codeToken));
                        var _urlHelper = _urlHelperFactory.GetUrlHelper(_actionContextAccessor.ActionContext);
                        var callbackUrl = _urlHelper.Page(
                            "/Account/ConfirmEmail", 
                            pageHandler: null, 
                            values: new { area = "Identity", userId = user.Id, code = codeToken }, 
                            protocol: _httpContextAccessor.HttpContext.Request.Scheme
                        );
                        
                        // Gửi mail thông tin tài khoản
                        await _sendMailService.SendMail(user.Email, "Đăng ký thành công!", $"Tài khoản: <a href=\"{HtmlEncoder.Default.Encode(callbackUrl)}\">{apiHoChieu.MaHoChieu.Trim()}</a> - Mật khẩu: {pass.Trim()}");

                        // Token JWT
                        var roles = await _userManager.GetRolesAsync(user);
                        var tokenString = JwtCommon.GetToken(_config, user, roles);

                        return new ApiSuccessResult<HoChieuHanhKhachView>(new HoChieuHanhKhachView
                        {
                            GioiTinh = apiHoChieu.GioiTinh,
                            HopThu = apiHoChieu.HopThu,
                            HoTen = apiHoChieu.HoTen,
                            MaHoChieu = apiHoChieu.MaHoChieu,
                            NgaySinh = apiHoChieu.NgaySinh,
                            TenQuocTich = apiHoChieu.TenQuocTich,
                            Token = tokenString,
                            NgayCapNhatCuoi = apiHoChieu.NgayCapNhatCuoi,
                            NgayTao = apiHoChieu.NgayTao,
                        }, "Đăng ký thành công!");
                    }
                    else
                    {
                        await _userManager.DeleteAsync(user);
                        await _hoChieuHanhKhachService.Delete(apiHoChieu.DuKhachID);
                    }
                }
                else
                {
                    await _userManager.DeleteAsync(user);
                }
            }
            return new ApiErrorResult<HoChieuHanhKhachView>("Đăng ký không thành công");
        }

        public async Task<bool> ChangeAvatar(IFormFile file)
        {
            string filenameOld = "";
            string userId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Sid);
            var user = await _userManager.FindByIdAsync(userId);

            string time = DateTime.Now.ToString("ddMMyyyyHHmmss");
            string _filename = time + "_" + Path.GetFileName(file.FileName);

            if (user != null)
            {
                if (!string.IsNullOrEmpty(user.Avatar))
                {
                    filenameOld = user.Avatar;
                }

                user.Avatar = _filename;
                var isSuccess = await _userManager.UpdateAsync(user);
                if (isSuccess.Succeeded)
                {
                    await _storageService.SaveFileAsync(file.OpenReadStream(), "Avatar", _filename);
                    if (!string.IsNullOrEmpty(filenameOld))
                        await _storageService.DeleteFileAsync("Avatar", filenameOld);
                    return true;
                }
            }
            return false;
        }

        public async Task<bool> ChangePassword(ChangePasswordParam request, HC_IdentityUser user)
        {
            var isSuccess = await _signInManager.CheckPasswordSignInAsync(user, request.OldPassword, true);

            if (isSuccess.Succeeded)
            {
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);

                var result = await _userManager.ResetPasswordAsync(user, code, request.Password);
                if (result.Succeeded)
                {
                    return true;
                }
            }

            return false;
        }

        public async Task<bool> ForgotPassword(ForgotPasswordParam request)
        {
            var code = await _userManager.GeneratePasswordResetTokenAsync(request.User);
            var result = await _userManager.ResetPasswordAsync(request.User, code, request.NewPassword);
            if (result.Succeeded)
            {
                // Gửi mail thông tin tài khoản
                await _sendMailService.SendMail(
                    request.User.Email, 
                    "Đổi mật khẩu", 
                    $"Tài khoản: {request.User.UserName.Trim()} - Mật khẩu: {request.NewPassword.Trim()}"
                );

                return true;
            }

            return false;
        }
    }
}
