﻿using Data.Models;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Data.SqlClient;
using System.Data;

namespace Services
{
    public interface INoiDungThamQuanService
    {
        Task<ApiResult<IEnumerable<NoiDungThamQuan>>> Gets();
        Task<ApiResult<NoiDungThamQuan>> GetByID(Guid id);
        Task<ApiResult<IEnumerable<NoiDungThamQuan>>> GetByDiemThamQuan(Guid id);
        Task<NoiDungThamQuan> Add(NoiDungThamQuan data);
        Task<NoiDungThamQuan> Edit(NoiDungThamQuan data);
        Task<int> DeleteByDiemThamQuanID(Guid id);
    }
    public class NoiDungThamQuanService : BaseService, INoiDungThamQuanService
    {
        public async Task<ApiResult<IEnumerable<NoiDungThamQuan>>> Gets()
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();

                    IEnumerable<NoiDungThamQuan> list = await conn.QueryAsync<NoiDungThamQuan>("SP_HC_NoiDungThamQuan_Gets", commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<IEnumerable<NoiDungThamQuan>>(list, "Lấy thông tin danh sách nội dung tham quan thành công!");
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<IEnumerable<NoiDungThamQuan>>("Có lỗi xảy ra: " + ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        public async Task<ApiResult<NoiDungThamQuan>> GetByID(Guid id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@NoiDungID", id);

                    NoiDungThamQuan list = await conn.QueryFirstOrDefaultAsync<NoiDungThamQuan>("SP_HC_NoiDungThamQuan_GetByID", parameters, commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<NoiDungThamQuan>(list, "Lấy thông tin danh sách nội dung tham quan thành công!");
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<NoiDungThamQuan>("Có lỗi xảy ra: " + ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<ApiResult<IEnumerable<NoiDungThamQuan>>> GetByDiemThamQuan(Guid id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@DiemThamQuanID", id);

                    IEnumerable<NoiDungThamQuan> list = await conn.QueryAsync<NoiDungThamQuan>("SP_HC_NoiDungThamQuan_GetByDiemThamQuan",parameters, commandType: CommandType.StoredProcedure);

                    return new ApiSuccessResult<IEnumerable<NoiDungThamQuan>>(list, "Lấy thông tin danh sách nội dung tham quan thành công!");
                }
                catch (Exception ex)
                {
                    return new ApiErrorResult<IEnumerable<NoiDungThamQuan>>("Có lỗi xảy ra: " + ex.Message);
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }

        public async Task<NoiDungThamQuan> Add(NoiDungThamQuan data)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@NgonNguID", data.NgonNguID);
                    parameters.Add("@DiemThamQuanID", data.DiemThamQuanID);
                    parameters.Add("@TenDiem", data.TenDiem);
                    parameters.Add("@DiaDiem", data.DiaDiem);
                    parameters.Add("@GioiThieu", data.GioiThieu);

                    NoiDungThamQuan? list = conn.QueryFirstOrDefault<NoiDungThamQuan>("SP_HC_NoiDungThamQuan_Add", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        public async Task<NoiDungThamQuan> Edit(NoiDungThamQuan data)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@NgonNguID", data.NgonNguID);
                    parameters.Add("@DiemThamQuanID", data.DiemThamQuanID);
                    parameters.Add("@TenDiem", data.TenDiem);
                    parameters.Add("@DiaDiem", data.DiaDiem);
                    parameters.Add("@GioiThieu", data.GioiThieu);
                    parameters.Add("@NoiDungID", data.NoiDungID);

                    NoiDungThamQuan? list = conn.QueryFirstOrDefault<NoiDungThamQuan>("SP_HC_NoiDungThamQuan_Edit", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
        public async Task<int> DeleteByDiemThamQuanID(Guid id)
        {
            using (SqlConnection conn = IConnectData())
            {
                try
                {
                    await conn.OpenAsync();
                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@DiemThamQuanID", id);

                    int list = conn.QueryFirstOrDefault<int>("SP_HC_NoiDungThamQuan_DeleteByDiemThamQuanID", parameters, commandType: CommandType.StoredProcedure);

                    return list;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                    }
                }
            }
        }
    }
}
