﻿using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace Services
{
    public interface IStorageService
    {
        string GetFileUrl(string folder, string fileName);
        Task SaveFileAsync(Stream mediaBinaryStream, string folder, string fileName);
        Task DeleteFileAsync(string folder, string fileName);
    }

    public class StorageService : BaseService, IStorageService
    {
        private readonly string _userContentFolder;
        private string USER_CONTENT_FOLDER_NAME;

        public StorageService(IHttpContextAccessor httpContextAccessor)
        {
            USER_CONTENT_FOLDER_NAME = $"FileUpload/{httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Sid)}";
            _userContentFolder = Path.Combine(Path.Combine(Directory.GetCurrentDirectory(),"wwwroot"), USER_CONTENT_FOLDER_NAME);
        }

        public string GetFileUrl(string folder, string fileName)
        {
            return $"/{_userContentFolder}/{folder}/{fileName}";
        }

        public async Task SaveFileAsync(Stream mediaBinaryStream, string folder, string fileName)
        {
            var path = Path.Combine(_userContentFolder, folder);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            var filePath = Path.Combine(path, fileName);
            using var output = new FileStream(filePath, FileMode.Create);
            await mediaBinaryStream.CopyToAsync(output);
        }

        public async Task DeleteFileAsync(string folder, string fileName)
        {
            var path = Path.Combine(_userContentFolder, folder);
            var filePath = Path.Combine(path, fileName);
            if (File.Exists(filePath))
            {
                await Task.Run(() => File.Delete(filePath));
            }
        }
    }
}
