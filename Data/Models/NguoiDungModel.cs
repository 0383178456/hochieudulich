﻿using System.ComponentModel.DataAnnotations;

namespace Data
{
    public class DangNhapModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Tài khoản không được bỏ trống!")]
        public string TaiKhoan { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Mật khẩu không được bỏ trống!")]
        public string MatKhau { get; set; }
        public bool GhiNho { get; set; }
    }

    public class DangKyModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Họ và tên không được bỏ trống!")]
        public string HoTen { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Giới tính không được bỏ trống!")]
        [Range(0, Int32.MaxValue, ErrorMessage = "Giới tính phải lớn hơn hoặc bằng 0!")]
        public byte? GioiTinh { get; set; } // 0: Nam, 1: Nữ, 2: Khác
        public string NgaySinh { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Hộp thư không được bỏ trống!")]
        [EmailAddress(ErrorMessage = "Hộp thư không đúng định dạng!")]
        public string HopThu { get; set; }
        public string SoDienThoai { get; set; }
        [Required(ErrorMessage = "Quốc tịch không được bỏ trống!")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Quốc tịch phải lớn hơn hoặc bằng 1!")]
        public int? QuocTich { get; set; }
    }

    public class ChangePasswordParam
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Mật khẩu không được bỏ trống!")]
        [StringLength(100, ErrorMessage = "Mật khẩu có ít nhất 6 kí tự và không vượt quá 100 kí tự!", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Mật khẩu không được bỏ trống!")]
        [StringLength(100, ErrorMessage = "Mật khẩu có ít nhất 6 kí tự và không vượt quá 100 kí tự!", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Xác nhận mật khẩu không được bỏ trống!")]
        [Compare("Password", ErrorMessage = "Mật khẩu không giống nhau!")]
        public string PasswordConfirm { get; set; }
    }

    public class ForgotPasswordParam
    {
        public HC_IdentityUser User { get; set; }
        public string NewPassword{ get; set; }
    }

    public class DangKyParam
    {
        public HC_IdentityUser User { get; set; }
    }

    public class ThongTinNguoiDung
    {
        public int ChuongTrinhThamGia { get; set; }
        public int DiaDiemDaCheckIn { get; set; }
        public int ThamGiaKyQuay { get; set; }
        public int TrungThuong { get; set; }
        public HoChieuHanhKhachView HoChieuHanhKhach { get; set; }
    }
}
