﻿using System.ComponentModel.DataAnnotations;

namespace Data.Models
{
    public class LichTrinh
    {
        public int LichTrinhID { get; set; }
        public Guid DiemThamQuanID { get; set; }
        public int ChuongTrinhID { get; set; }
        public int ThuTu { get; set; }
    }
    public class LichTrinhParam
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Điểm tham quan không được bỏ trống!")]
        public string DiemThamQuanID { get; set; }

        [Required(ErrorMessage = "Chương trình không được bỏ trống!")]
        public int ChuongTrinhID { get; set; }

        [Required(ErrorMessage = "Thứ tự không được bỏ trống!")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Thứ tự phải lớn hơn hoặc bằng 1!")]
        public int ThuTu { get; set; }
    }
    public class LichTrinhChuongTrinhModel
    {
        // Lịch trình
        public int LichTrinhID { get; set; }
        public int ThuTuLichTrinh { get; set; }
        // Chương trình
        public int ChuongTrinhID { get; set; }
        public string GioiThieuChuongTrinh { get; set; }
        public string TenChuongTrinh { get; set; }
        // Điểm tham quan
        public Guid DiemThamQuanID { get; set; }
        public string TenDiem { get; set; }
        public string DiaDiem { get; set; }
        public string GioiThieuDiemThamQuan { get; set; }
        public double KinhDo { get; set; }
        public double ViDo { get; set; }
        public int BanKinhQuyUoc { get; set; }
        public int ThuTuDiemThamQuan { get; set; }
        public string AnhDaiDien { get; set; }
        public bool IsCheckIn { get; set; }
    }
}
