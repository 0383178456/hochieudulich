﻿using System.ComponentModel.DataAnnotations;

namespace Data
{
    public class QuocTich
    {
        public int QuocTichID { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Tên quốc tịch không được bỏ trống!")]
        public string TenQuocTich { get; set; }
    }

    public class QuocTichCheck
    {
        public int QuocTichID { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Tên quốc tịch không được bỏ trống!")]
        public string TenQuocTich { get; set; }
        public int Action { get;set; }
    }
}
