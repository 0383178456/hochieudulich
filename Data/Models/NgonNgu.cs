﻿using System.ComponentModel.DataAnnotations;

namespace Data
{
    public class NgonNgu
    {
        public int NgonNguID { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Tên ngôn ngữ không được bỏ trống!")]
        public string TenNgonNgu { get; set; }
    }

    public class NgonNguCheck
    {
        public int? NgonNguID { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Tên ngôn ngữ không được bỏ trống!")]
        public string TenNgonNgu { get; set; }
        public int Action { get; set; }
    }
}
