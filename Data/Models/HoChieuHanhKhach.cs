﻿using System.ComponentModel.DataAnnotations;

namespace Data
{
    public class HoChieuHanhKhach
    {
        public Guid DuKhachID { get; set; }
        public string HoTen { get; set; }
        public string MaHoChieu { get; set; }
        public byte? GioiTinh { get; set; }
        public string HopThu { get; set; }
        public DateTime? NgaySinh { get; set; }
        public DateTime NgayTao { get; set; }
        public DateTime NgayCapNhatCuoi { get; set; }
        public int? QuocTichID { get; set; }
        public string TenQuocTich { get; set; }
    }
    public class HoChieuHanhKhachParam
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Họ và tên không được bỏ trống!")]
        public string HoTen { get; set; }
        public byte? GioiTinh { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Hộp thư không được bỏ trống!")]
        [EmailAddress(ErrorMessage = "Hộp thư không đúng định dạng!")]
        public string HopThu { get; set; }
        public string NgaySinh { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Quốc tịch không được bỏ trống!")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Quốc tịch phải lớn hơn hoặc bằng 1!")]
        public int? QuocTichID { get; set; }
    }
    public class HoChieuHanhKhachUpdateParam : HoChieuHanhKhachParam
    {
        public Guid DuKhachID { get; set; }
    }
    public class HoChieuHanhKhachModel
    {
        public Guid DuKhachID { get; set; }
        public string HoTen { get; set; }
        public string MaHoChieu { get; set; }
        public byte? GioiTinh { get; set; }
        public string HopThu { get; set; }
        public DateTime? NgaySinh { get; set; }
        public DateTime NgayTao { get; set; }
        public DateTime NgayCapNhatCuoi { get; set; }
        public int? QuocTichID { get; set; }
    }
    public class HoChieuHanhKhachView
    {
        public Guid? DuKhachID { get; set; }
        public string HoTen { get; set; }
        public string MaHoChieu { get; set; }
        public byte? GioiTinh { get; set; }
        public string HopThu { get; set; }
        public DateTime? NgaySinh { get; set; }
        public DateTime NgayTao { get; set; }
        public DateTime NgayCapNhatCuoi { get; set; }
        public string TenQuocTich { get; set; }
        public int? QuocTichID { get; set; }
        public string Token { get; set; }
    }
    public class HoChieuHanhKhachFilter
    {
        public string? HoTen { get; set; }
    }
}
