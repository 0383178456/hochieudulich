﻿namespace Data.Models
{
    public class NguoiQuaySo
    {
        public Guid NguoiQuaySoID { get; set; }
        public long KyQuayID { get; set; }
        public Guid DuKhachID { get; set; }
        public bool TrungThuong { get; set; }
    }
}
