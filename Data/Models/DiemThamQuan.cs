﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class DiemThamQuan
    {
        public Guid DiemThamQuanID { get; set; }
        public float KinhDo { get; set; }
        public float ViDo { get; set; }
        public int BanKinhQuyUoc { get; set; }
        public int ThuTu { get; set; }
        public string AnhDaiDien { get; set; }
        public Guid NoiDungID { get; set; }
        public int NgonNguID { get; set; }
        public string TenDiem { get; set; }
        public string DiaDiem { get; set; }
        public string GioiThieu { get; set; }
        public string TenNgonNgu { get; set; }
    }

    public class DiemThamQuanParam
    {
        public Guid DiemThamQuanID { get; set; }
        public float KinhDo { get; set; }
        public float ViDo { get; set; }
        public int BanKinhQuyUoc { get; set; }
        public int ThuTu { get; set; }
        public string AnhDaiDien { get; set; }
    }

    public class DiemThamQuanSearch
    {
        public string? TuKhoa { get; set; }
        public int NgonNguID { get; set; }
    }
}
