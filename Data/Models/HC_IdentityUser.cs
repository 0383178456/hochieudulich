﻿using Microsoft.AspNetCore.Identity;

namespace Data
{
    public class HC_IdentityUser : IdentityUser
    {
        public Guid HoChieuDuKhachID { get; set; }
        public string? Avatar { get; set; }
        public string? Token { get; set; }
        public string? RefreshToken { get; set; }
        public string? Fullname { get; set; }
    }
}
