﻿using System.ComponentModel.DataAnnotations;

namespace Data.Models
{
    public class KyQuaySo
    {
        public long KyQuayID { get; set; }
        public string TenKyQuay { get; set; }
        public DateTime NgayQuayThuong { get; set; }
        public DateTime ThoiGianTu { get; set; }
        public DateTime ThoiGianDen { get; set; }
    }
    public class KyQuaySoParam
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Tên kỳ quay không được bỏ trống!")]
        public string TenKyQuay { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Ngày quay thưởng không được bỏ trống!")]
        public string NgayQuayThuong { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Thời gian từ không được bỏ trống!")]
        public string ThoiGianTu { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Thời gian đến không được bỏ trống!")]
        public string ThoiGianDen { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Thời gian đến không được bỏ trống!")]
        public string DuKhachID { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Thời gian đến không được bỏ trống!")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Chương trình phải lớn hơn hoặc bằng 1!")]
        public int ChuongTrinhID { get; set; }
    }

    public class KyQuaySoInfo
    {
        public long KyQuayID { get; set; }
        public string TenKyQuay { get; set; }
        public DateTime NgayQuayThuong { get; set; }
        public DateTime ThoiGianTu { get; set; }
        public DateTime ThoiGianDen { get; set; }
        public Guid NguoiQuaySoID { get; set; }
        public Guid DuKhachID { get; set; }
        public bool TrungThuong { get; set; }
    }
}
