﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class NoiDungThamQuan
    {
        public Guid NoiDungID { get; set; }
        public int NgonNguID { get; set; }
        public Guid DiemThamQuanID { get; set; }
        public string TenDiem { get; set; }
        public string DiaDiem { get; set; }
        public string GioiThieu { get; set; }
    }
}
