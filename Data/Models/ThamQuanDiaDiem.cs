﻿using System.ComponentModel.DataAnnotations;

namespace Data.Models
{
    public class ThamQuanDiaDiem
    {
        public Guid DiemDaDiQuaID { get; set; }
        public Guid DuKhachID { get; set; }
        public DateTime ThoiGianThamQuan { get; set; }
        public int LichTrinhID { get; set; }
        public Guid DiemThamQuanID { get; set; }
        public int ChuongTrinhID { get; set; }
    }

    public class ThamQuanDiaDiemParam
    {
        [Required(AllowEmptyStrings = true, ErrorMessage = "Lịch trình không được bỏ trống!")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Lịch trình phải lớn hơn hoặc bằng 1!")]
        public int LichTrinhID { get; set; }
    }

    public class ThamQuanDiaDiemChuongTrinhModel
    {
        public int ChuongTrinhID { get; set; }
        public Guid DuKhachID { get; set; }
    }

    public class ThamQuanDiaDiemChuongTrinhDiemThamQuanModel
    {
        public int ChuongTrinhID { get; set; }
        public Guid DuKhachID { get; set; }
        public Guid DiemThamQuanID { get; set; }
    }
    public class ThamQuanDiaDiemTheoChuongTrinhModel
    {
        public LichTrinhChuongTrinhModel LichTrinh { get; set; }
        public ThamQuanDiaDiem? ThamQuanDiaDiem { get; set; }
    }
}
