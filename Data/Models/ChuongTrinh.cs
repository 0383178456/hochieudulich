﻿namespace Data
{
    public class ChuongTrinhGet
    {
        public int ChuongTrinhID { get; set; }
        public string TenChuongTrinh { get; set; }
        public string GhiChu { get; set; }
        public DateTime NgayCapNhatCuoi { get; set; }
    }

    public class ChuongTrinh
    {
        public int ChuongTrinhID { get; set; }
        public string TenChuongTrinh { get; set; }
        public string GhiChu { get; set; }
        public string NgayCapNhatCuoi { get; set; }
    }
}
