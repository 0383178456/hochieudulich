﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class TepKemTheo
    {
        public Guid TepID { get; set; }
        public Guid ChuTheID { get; set; }
        public string MoTa { get; set;}
        public string LienKet { get; set;}
        public int Kieu { get; set; }
    }

    public class TepKemTheoParam
    {
        public Guid ChuTheID { get; set; }
        public string MoTa { get; set; }
        public string LienKet { get; set; }
    }
}
