﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public class HCDbContext : IdentityDbContext<HC_IdentityUser, IdentityRole, string>
    {
        public HCDbContext(DbContextOptions<HCDbContext> options)
            : base(options)
        {
        }

        // Rename table database
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            foreach (var entityType in builder.Model.GetEntityTypes())
            {
                var tableName = entityType.GetTableName();

                if (tableName != null && tableName.StartsWith("AspNet"))
                {
                    entityType.SetTableName(tableName.Replace("AspNet", "HC_"));
                }
            }
        }
    }
}
